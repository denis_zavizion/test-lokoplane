<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_history', function (Blueprint $table) {
//            $table->id('buy_id');
            $table->string('buy_id', 255);
            $table->integer('order_id')->nullable();
            $table->integer('book_id')->nullable();
            $table->text('status')->nullable();
            $table->text('pnr_number')->nullable();
            $table->text('amount')->nullable();
            $table->text('email')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('phone')->nullable();
            $table->text('country')->nullable();
            $table->text('city')->nullable();
            $table->text('address')->nullable();
            $table->text('zip')->nullable();
            $table->text('first_name')->nullable();
            $table->text('last_name')->nullable();
            $table->text('middle_name')->nullable();
            $table->text('birth_date')->nullable();
            $table->text('citizenship')->nullable();
            $table->text('expire')->nullable();
            $table->text('extra_phone')->nullable();
            $table->text('bilet')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_history');
    }
}
