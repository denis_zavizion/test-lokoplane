## Version
node -v
*v10.0.0*

npm -v
*5.6.0*

php -v
*7.2.26*

php artisan --version Laravel
*laravel 7.2.2*

---

## Install this project


Install project

1)git clone https://denis_zavizion@bitbucket.org/denis_zavizion/front.gosee.git

2)service nginx restart

3)cd www

4)
composer global config bin-dir --absolute

composer update

5)sudo chmod -R 777 storage

6)npm install vue

7).env прописать DB

8)php artisan key:generate

9)php artisan migrate

10) npm run watch-poll
---

##cache

php artisan cache:clear

php artisan config:clear

php artisan route:clear

php artisan view:clear


