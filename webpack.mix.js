const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css/app.css');

mix.sass('resources/assets/sass/auth.scss', 'public/css/auth.css');

mix.styles(['resources/assets/ihover/styles/main.css'], 'public/css/ihover.css');
mix.js('resources/assets/js/theme/html5shiv.js', 'public/js/html5shiv.js');
mix.js(['resources/assets/js/theme/script.js'], 'public/js/script.js');
