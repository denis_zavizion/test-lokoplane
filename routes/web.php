<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('indexPage');
Route::get('/ajax/getJson', 'IndexController@getJson')->name('getJson');
Route::post('/ajax/getCities', 'AirTicketsController@arrCities')->name('getCities');

Auth::routes();
Route::get('/cabinet', 'CabinetController@index');
Route::post('/avatar_upload', 'CabinetController@AvatarUpload')->name('avatar.upload.post');

Route::post('/doc_cabinet_1', 'CabinetController@doc_cabinet_1')->name('doc_cabinet_1');
Route::post('/doc_cabinet_2', 'CabinetController@doc_cabinet_2')->name('doc_cabinet_2');

Route::get('logout','Auth\LoginController@logout');
Route::get('/post/{slug}', 'PostsController@index')->name('post');
Route::get('/order/{id}', 'AirTicketsController@order')->name('order');

Route::get('/air-tickets/', 'AirTicketsController@airTickets')->name('airTickets');

Route::get('/air/tickets/buy/', 'AirTicketsController@buy')->name('buy');
Route::get('/air/tickets/buy_info/', 'AirTicketsController@buy_info')->name('buy_info');
Route::post('/ajax-air-tickets/', 'AirTicketsController@ajaxAirTickets')->name('ajaxAirTickets');
Route::post('/postAjaxSearchResult/', 'AirTicketsController@postAjaxSearchResult')->name('postAjaxSearchResult');
Route::post('/postAjaxSearchAvia/', 'AirTicketsController@postAjaxSearchAvia')->name('postAjaxSearchAvia');
Route::post('/postAjaxTickets/', 'AirTicketsController@postAjaxTickets')->name('postAjaxTickets');
Route::post('/payment', 'AirTicketsController@payment');

Route::get('/corp-landing', 'IndexController@corpLanding')->name('corplanding');
Route::get('/taplink', 'IndexController@taplink')->name('taplink');
