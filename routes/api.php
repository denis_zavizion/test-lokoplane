<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', 'Auth\api\LoginController@login');
Route::post('/register', 'Auth\api\LoginController@register');



//Route::get('/user', function () {
//    //
//})->middleware('auth:api');
Route::get('/cabinet', 'Auth\api\CabinetController@index');
Route::post('/avatar_upload', 'Auth\api\CabinetController@AvatarUpload');
Route::post('/doc_cabinet', 'Auth\api\CabinetController@doc_cabinet');

Route::post('/payment_history_pennding', 'Auth\api\BiletController@payment_history_pennding');
Route::post('/payment_history_active', 'Auth\api\BiletController@payment_history_active');
Route::post('/payment_history_bilet', 'Auth\api\BiletController@payment_history_bilet');
Route::post('/postAjaxTickets', 'Auth\api\BiletController@postAjaxTickets');
Route::post('/postAjaxTicketsResult', 'Auth\api\BiletController@postAjaxTicketsResult');
