jQuery(document).ready(function ($) {
    $(document).on('click', function (event) {
        if (!$(event.target).closest(".dropdown_input").length) {
            $('.dropdown_block').fadeOut(200);
        }
    });
    $('.load_more_btn').click(function () {
        $(this).siblings('.more_checkboxes').slideToggle(200);
    });
    $('.user_icon, .modal_overlay, .form_title .close').click(function () {
        $('.form_modal').fadeToggle(200);
        $('.modal_overlay').fadeToggle(200);
    });
    $('.change_login').click(function () {
        $('.login_form').hide();
        $('.register_form').fadeIn(200);
    });
    $('.change_register').click(function () {
        $('.register_form').hide();
        $('.login_form').fadeIn(200);
    });
    $('.dropdown_item').click(function () {
        $(this).parents('.dropdown_input').find('.hidden_input').val($(this).data('input-value'));
        $(this).parents('.dropdown_input').find('.dropdown_title .search_input').val($(this).find('h4').text());
        $(this).parents('.dropdown_block').find('.from_to_input').val($(this).find('p').text());
        $(this).parents('.dropdown_input').find('.dropdown_title span').text($(this).find('p').text());
        $(this).parents('.dropdown_block').fadeOut(200);
    });
    $('.filter_block .dropdown_item').click(function () {
        $(this).parents('.dropdown_input').find('.search_input').val($(this).find('h4').text());
        $(this).parents('.dropdown_block').fadeOut(200);
    });
    $('.dropdown_title').click(function () {
        $('.dropdown_block').hide();
        $(this).next('.dropdown_block').fadeIn(200);
    });
    $('.tabs .tab_item').click(function () {
        $('.tabs .tab_item').removeClass('active');
        $(this).addClass('active');
        $('.cabinet_main_block').removeClass('active_cabinet_tab');
        $('.cabinet_main_block[data-tab=' + $(this).data('tab-open') + ']').addClass('active_cabinet_tab');
        $('.cabinet_main_block[data-tab=' + $(this).data('tab-open') + ']').addClass('active_cabinet_tab');
    });
    $('.plus').click(function () {
        $(this).siblings('input').val(parseInt($(this).siblings('input').val()) + 1);
        $('.types_dropdown span[data-text=' + $(this).data('text-output') + ']').text($(this).siblings('input').val());
    });
    $('.name .close').click(function () {
        $(this).parents('.name').remove();
    });
    $('.minus').click(function () {
        if ($(this).siblings('input').val() >= 1) {
            $(this).siblings('input').val(parseInt($(this).siblings('input').val()) - 1);
        }

        $('.types_dropdown span[data-text=' + $(this).data('text-output') + ']').text($(this).siblings('input').val());
    });
    $('label[for=railway]').click(function () {
        $('.cloud.railway, .shape_mobile.railway, .tram_btn').show();
        $('.cloud.second, .cloud.first,.cloud.third,.types_dropdown,.plane_btn, .plane_shape').hide();
        $(this).parents('.inner_search').addClass('header_search_rail_bg');
    });
    $('label[for=avia]').click(function () {
        $('.plane_btn, .cloud.second, .cloud.first,.cloud.third,.types_dropdown, .plane_shape').show();
        $('.cloud.railway, .shape_mobile.railway, .tram_btn').hide();
        $(this).parents('.inner_search').removeClass('header_search_rail_bg');
    });
    $('.radio_btns label').click(function () {
        $('.trip_type.last').text($(this).text());
    });
    $('.filter_title').click(function () {
        $(this).toggleClass('hide');
        $(this).siblings('.filter_block').slideToggle(200);
    });
    $('.mobile_filter_btn').click(function () {
        $(this).toggleClass('active');
        $('.left_filter').slideToggle(400);

        if ($(this).hasClass('active')) {
            $(this).find('p').text('скрыть фильтр');
        } else {
            $(this).find('p').text('раскрыть фильтр');
        }
    });
    $('.exchange').click(function () {
        var from_input = $('input[name = from_input]').val();
        var to_input = $('input[name = to_input]').val();
        var first_input_val = $(this).parents('.banner_input').find('.hidden_input').val();
        var second_input_val = $(this).parents('.banner_input').next('.banner_input').find('.hidden_input').val();
        $(this).parents('.banner_input').find('.hidden_input').val(second_input_val);
        $(this).parents('.banner_input').next('.banner_input').find('.hidden_input').val(first_input_val);
        var first_input_title = $(this).parents('.banner_input').find('.dropdown_title .search_input').val();
        var first_input_subtitle = $(this).parents('.banner_input').find('.dropdown_title span').text();
        var second_input_title = $(this).parents('.banner_input').next('.banner_input').find('.dropdown_title .search_input').val();
        var second_input_subtitle = $(this).parents('.banner_input').next('.banner_input').find('.dropdown_title span').text();
        $(this).parents('.banner_input').find('.dropdown_title .search_input').val(second_input_title);
        $(this).parents('.banner_input').find('.dropdown_title span').text(second_input_subtitle);
        $(this).parents('.banner_input').next('.banner_input').find('.dropdown_title .search_input').val(first_input_title);
        $(this).parents('.banner_input').next('.banner_input').find('.dropdown_title span').text(first_input_subtitle);
        $('input[name = from_input]').val(to_input);
        $('input[name = to_input]').val(from_input);
    });
    $('.main_section label[for=avia]').click(function () {
        $(this).parents('.main_section').find('.plane_btn').show();
        $(this).parents('.main_section').find('.railway_btn').hide();
    });
    $('.main_section label[for=railway]').click(function () {
        $(this).parents('.main_section').find('.plane_btn').hide();
        $(this).parents('.main_section').find('.railway_btn').show();
    });
    $('.inner_search label[for=avia]').click(function () {
        $(this).parents('.inner_search').find('.plane_btn').show();
        $(this).parents('.inner_search').find('.railway_btn').hide();
    });
    $('.inner_search label[for=railway]').click(function () {
        $(this).parents('.inner_search').find('.plane_btn').hide();
        $(this).parents('.inner_search').find('.railway_btn').show();
    });
    $('.lang_title').click(function () {
        $('.lang_dropdown_block').fadeToggle(200);
    });
    $('.one_side').change(function () {
        if ($(this).is(':checked')) {
            $('.finish_block').hide();
        }
    });
    $('.two_side').change(function () {
        if ($(this).is(':checked')) {
            $('.finish_block').show();
        }
    });
    $('.search_input').on('input', function () {
        var search_text = $(this).val().toUpperCase();
        if(search_text.length>2){
            $(this).parents('.dropdown_input').find('.dropdown_item').each(function () {
                if ($(this).data('search-keyword').toString().toUpperCase().indexOf(search_text) >= 0) {
                    // alert($(this).data('search-keyword').toString().toUpperCase().indexOf(search_text))
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        };
    });

    if ($(window).width() < 576) {
        $('footer .main_footer_menu h4').click(function () {
            $(this).toggleClass('active');
            $(this).next('.menu_list').slideToggle(400);
        });
    }

    $('.phone_input .dropdown_item').click(function () {
        $(this).parents('.phone_input').find('.phone').val('');
        $(this).parents('.phone_input').find('.dropdown_title .search_input').val($(this).data('input-value'));
        $(this).parents('.phone_input').find('.phone').attr('placeholder', $(this).data('placeholder'));
        $(this).parents('.phone_input').find('.phone').mask($(this).data('country-num'), {
            clearMaskOnLostFocus: false
        });
    });
    $("#submit_pay").on("click", function () {
        $("#loading").show();
        $("#book_now").hide();
    });
});
