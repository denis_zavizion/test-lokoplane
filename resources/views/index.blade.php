{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--    <link rel="icon" type="image/png" href="img/favicon.jpg">--}}
{{--    <meta charset="UTF-8">--}}
{{--    <meta content="IE=edge" http-equiv="X-UA-Compatible">--}}
{{--    <meta content="width=device-width, initial-scale=1" name="viewport">--}}
{{--<!--[if lt IE 9]>--}}
{{--        <script src="{{ asset('js/html5shiv.js') }}"></script>--}}
{{--      <![endif]-->--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('css/libs.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('css/media.css') }}">--}}

{{--    <title>Document</title>--}}

{{--</head>--}}
{{--<body>--}}
{{--<div class="modal_overlay"></div>--}}
{{--<div class="call_btn">--}}
{{--    <img src="{{ asset('img/call.svg') }}">--}}
{{--</div>--}}
{{--<header class="main_header">--}}
{{--    <div class="row">--}}
{{--        <a href="#" class="logo_block">--}}
{{--            <h3 class="logo_text"><span>loko</span>plane</h3>--}}
{{--        </a>--}}
{{--        <div class="user_block">--}}
{{--            <img src="{{ asset('img/user.svg') }}" class="user_icon">--}}
{{--            <div class="form_modal">--}}
{{--                <div class="form_tab login_form active">--}}
{{--                    <div class="form_title">--}}
{{--                        <h4>{{ __('messages.login_title') }}</h4>--}}
{{--                        <img src="{{ asset('img/close.svg') }}" class="close">--}}
{{--                    </div>--}}
{{--                    <div class="form_content">--}}
{{--                        <div class="inputs_block">--}}
{{--                            <div class="input_block">--}}
{{--                                <p>{{ __('messages.form_email') }}</p>--}}
{{--                                <input type="text">--}}
{{--                            </div>--}}
{{--                            <div class="input_block forgot_password">--}}
{{--                                <p>{{ __('messages.password') }}:</p>--}}
{{--                                <input type="text">--}}
{{--                                <img src="{{ asset('img/show.svg') }}" class="show">--}}
{{--                            </div>--}}
{{--                            <a href="#" class="forgot_password_btn">{{ __('messages.forgot_password') }}</a>--}}
{{--                        </div>--}}
{{--                        <div class="form_btns">--}}
{{--                            <button class="btn submit_btn">{{ __('messages.login_btn') }}</button>--}}
{{--                            <button class="btn fb_btn" type="button">--}}
{{--                                <img src="{{ asset('img/fb.svg') }}">--}}
{{--                                {{ __('messages.facebook_btn') }}--}}
{{--                            </button>--}}
{{--                            <button class="btn google_btn" type="button">--}}
{{--                                <img src="{{ asset('img/google.svg') }}">--}}
{{--                                {{ __('messages.google_btn') }}--}}
{{--                            </button>--}}
{{--                            <button class="btn vk_btn" type="button">--}}
{{--                                <img src="{{ asset('img/vk.svg') }}">--}}
{{--                                {{ __('messages.vk_btn') }}--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                        <button type="button" class="change_tab_btn change_login">{{ __('messages.create_title') }}</button>--}}


{{--                    </div>--}}

{{--                </div>--}}
{{--                <div class="form_tab register_form">--}}
{{--                    <div class="form_title">--}}
{{--                        <h4>{{ __('messages.create_title') }}</h4>--}}
{{--                        <img src="{{ asset('img/close.svg') }}" class="close">--}}
{{--                    </div>--}}
{{--                    <div class="form_content">--}}
{{--                        <div class="input_block">--}}
{{--                            <p>{{ __('messages.name') }}</p>--}}
{{--                            <input type="text">--}}
{{--                        </div>--}}
{{--                        <div class="input_block">--}}
{{--                            <p>{{ __('messages.form_email') }}</p>--}}
{{--                            <input type="text">--}}
{{--                        </div>--}}
{{--                        <p class="password_msg">{{ __('messages.password_hint') }}</p>--}}
{{--                        <div class="input_block forgot_password">--}}
{{--                            <p>{{ __('messages.password') }}:</p>--}}
{{--                            <input type="text">--}}
{{--                            <img src="{{ asset('img/show.svg') }}" class="show">--}}
{{--                        </div>--}}
{{--                        <div class="input_block forgot_password">--}}
{{--                            <p>{{ __('messages.password_2') }}:</p>--}}
{{--                            <input type="text">--}}
{{--                            <img src="{{ asset('img/show.svg') }}" class="show">--}}
{{--                        </div>--}}
{{--                        <div class="form_btns">--}}
{{--                            <button class="btn submit_btn">{{ __('messages.create') }}</button>--}}
{{--                        </div>--}}
{{--                        <button type="button" class="change_tab_btn change_register">{{ __('messages.login_title') }}</button>--}}


{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="menu_block">--}}
{{--            <img src="{{ asset('img/main-menu.svg') }}">--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</header>--}}
{{--<section class="main_section" id="app">--}}
{{--    <form class="banner_content row">--}}
{{--        <h2 class="section_title">{{ __('messages.main_page_title') }}</h2>--}}
{{--        <div class="toggler">--}}
{{--            <input type="radio" name="search_type" id="avia" value="avia" checked>--}}

{{--            <label for="avia" class="first">--}}
{{--                <img src="{{ asset('img/plane.svg') }}"><span>{{ __('messages.airplane_btn') }}</span>--}}
{{--            </label>--}}

{{--            <input type="radio" name="search_type" id="railway" value="railway">--}}
{{--            <label for="railway">--}}
{{--                <img src="{{ asset('img/tram.svg') }}"><span>{{ __('messages.railway_btn') }}</span>--}}
{{--            </label>--}}


{{--        </div>--}}
{{--        <div class="input_row">--}}
{{--            <button type="button" class="additional_btn plane_btn" style="display: block">{{ __('messages.multiple_flights') }}</button>--}}
{{--            <button type="button" class="additional_btn tram_btn">{{ __('messages.timetable') }}</button>--}}
{{--            <div class="banner_input dropdown_input where_from">--}}
{{--                <img src="{{ asset('img/exchange.svg') }}" class="exchange hide_md">--}}
{{--                <img src="{{ asset('img/exchange_mobile.svg') }}" class="exchange show_md">--}}

{{--                <p>{{ __('messages.from') }}</p>--}}
{{--                <div class="dropdown_title">--}}
{{--                    <input type="text" class="search_input city_input">--}}
{{--                    <span></span>--}}
{{--                </div>--}}
{{--                <div class="dropdown_block">--}}
{{--                    <div class="dropdown_list">--}}
{{--                        @foreach($cities as $key=>$city)--}}
{{--                            <div class="dropdown_item" data-input-value="{{$city}}" data-search-keyword="{{$city}}, {{$key}}">--}}
{{--                                <h4>{{$city}}</h4>--}}
{{--                                <p>{{$key}}</p>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
{{--                    <input type="hidden" class="hidden_input">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="banner_input dropdown_input where_from where_input">--}}
{{--                <p>{{ __('messages.where') }}</p>--}}
{{--                <div class="dropdown_title">--}}
{{--                    <input type="text" class="search_input city_input">--}}
{{--                    <span></span>--}}
{{--                </div>--}}
{{--                <div class="dropdown_block">--}}
{{--                    <div class="dropdown_list">--}}
{{--                        @foreach($cities as $key=>$city)--}}
{{--                            <div class="dropdown_item" data-input-value="{{$city}}" data-search-keyword="{{$city}}, {{$key}}">--}}
{{--                                <h4>{{$city}}</h4>--}}
{{--                                <p>{{$key}}</p>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
{{--                    <input type="hidden" class="hidden_input">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="banner_input start date">--}}
{{--                <p>{{ __('messages.start_route') }}</p>--}}
{{--                <input type="text" id="beginning">--}}
{{--                <img src="{{ asset('img/calendar.svg') }}" class="icon">--}}
{{--            </div>--}}
{{--            <div class="banner_input date finish_block" style="display: none;">--}}
{{--                <p>{{ __('messages.end_route') }}</p>--}}
{{--                <img src="{{ asset('img/calendar.svg') }}" class="icon">--}}
{{--                <input type="text" id="end">--}}
{{--            </div>--}}
{{--            <div class="banner_input dropdown_input types_dropdown">--}}
{{--                <p>{{ __('messages.amount') }}</p>--}}
{{--                <div class="dropdown_title">--}}
{{--                    <span data-text="adults">1 </span> {{ __('messages.adults_short') }},--}}
{{--                    <span data-text="child">0 </span> {{ __('messages.children_short') }},--}}
{{--                    <span data-text="baby">0 </span> {{ __('messages.babies_short') }},--}}
{{--                    <span class="type trip_type last">--}}
{{--								{{ __('messages.econom') }}--}}
{{--							</span>--}}

{{--                </div>--}}
{{--                <div class="dropdown_block">--}}
{{--                    <div class="dropdown_content">--}}
{{--                        <div class="item">--}}
{{--                            <div class="item_text">--}}
{{--                                <h4>{{ __('messages.adults') }}</h4>--}}
{{--                                <p>{{ __('messages.older_12') }}</p>--}}
{{--                            </div>--}}
{{--                            <div class="amount">--}}
{{--                                <button type="button" class="minus" data-text-output="adults">-</button>--}}
{{--                                <input name="adults" type="text" value="1" readonly>--}}
{{--                                <button type="button" class="plus" data-text-output="adults">+</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="item">--}}
{{--                            <div class="item_text">--}}
{{--                                <h4>{{ __('messages.children') }}</h4>--}}
{{--                                <p>{{ __('messages.younger_12') }}</p>--}}
{{--                            </div>--}}
{{--                            <div class="amount">--}}
{{--                                <button type="button" class="minus" data-text-output="child">-</button>--}}
{{--                                <input name="child" type="text" value="0" readonly>--}}
{{--                                <button type="button" class="plus" data-text-output="child">+</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="item">--}}
{{--                            <div class="item_text">--}}
{{--                                <h4>{{ __('messages.babies') }}</h4>--}}
{{--                                <p>{{ __('messages.younger_2') }}</p>--}}
{{--                            </div>--}}
{{--                            <div class="amount">--}}
{{--                                <button type="button" class="minus" data-text-output="baby">-</button>--}}
{{--                                <input name="baby" type="text" value="0" readonly>--}}
{{--                                <button type="button" class="plus" data-text-output="baby">+</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="radio_btns">--}}
{{--                        <div class="radio_item">--}}
{{--                            <input name="price_type" value="E" type="radio" id="radio1" checked>--}}
{{--                            <label for="radio1">{{ __('messages.econom') }}</label>--}}
{{--                        </div>--}}
{{--                        <div class="radio_item">--}}
{{--                            <input name="price_type" value="W" type="radio" id="radio2">--}}
{{--                            <label for="radio2">{{ __('messages.business_class') }}</label>--}}
{{--                        </div>--}}
{{--                        <div class="radio_item">--}}
{{--                            <input name="price_type" value="B" type="radio" id="radio3">--}}
{{--                            <label for="radio3">{{ __('messages.first') }}</label>--}}
{{--                        </div>--}}
{{--                        <div class="radio_item">--}}
{{--                            <input name="price_type" value="F" type="radio" id="radio4">--}}
{{--                            <label for="radio4">{{ __('messages.search_all') }}</label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="radios_block">--}}
{{--            <div class="radio_item">--}}
{{--                <input type="radio" id="radio5" name="type"  class="two_side" >--}}
{{--                <label for="radio5">{{ __('messages.two_ways') }}</label>--}}
{{--            </div>--}}
{{--            <div class="radio_item">--}}
{{--                <input type="radio" id="radio6" name="type" class="one_side" checked>--}}
{{--                <label for="radio6">{{ __('messages.one_way') }}</label>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <button type="submit" class="submit_btn">--}}
{{--            <div class="plane_btn">--}}
{{--                {{ __('messages.airplane_btn') }}--}}
{{--                <img src="{{ asset('img/plane.svg') }}">--}}
{{--            </div>--}}
{{--            <div class="railway_btn">--}}
{{--                {{ __('messages.railway_btn') }}--}}
{{--                <img src="{{ asset('img/tram2.svg') }}">--}}
{{--            </div>--}}

{{--        </button>--}}
{{--    </form>--}}
{{--    <img src="{{ asset('img/clouds3.svg') }}" class="cloud first">--}}
{{--    <img src="{{ asset('img/clouds2.svg') }}" class="cloud second">--}}
{{--    <img src="{{ asset('img/clouds1.svg') }}" class="cloud third">--}}
{{--    <img src="{{ asset('img/visuals.png') }}" class="cloud railway">--}}
{{--    <img src="{{ asset('img/railway_bg_mobile.svg') }}" class="shape_mobile railway">--}}
{{--    <img src="{{ asset('img/plane_bg_mobile.svg') }}" class="shape_mobile plane_shape">--}}
{{--</section>--}}
{{--<section class="cards_section">--}}
{{--    <div class="container">--}}
{{--        <h2 class="section_title">{{ __('messages.loyalty_title') }}</h2>--}}
{{--        <div class="row">--}}
{{--            <div class="card">--}}
{{--                <img src="{{ asset('img/ultra.png') }}">--}}
{{--            </div>--}}
{{--            <div class="card">--}}
{{--                <img src="{{ asset('img/gold.png') }}">--}}
{{--            </div>--}}
{{--            <div class="card">--}}
{{--                <img src="{{ asset('img/silver.png') }}">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--<footer class="main_footer">--}}
{{--    <div class="first_footer">--}}
{{--        <div class="container">--}}
{{--            <div class="main_footer_menu row">--}}
{{--                --}}{{--                <div class="menu_block">--}}
{{--                --}}{{--                    <h4>{{ __('messages.countries') }}</h4>--}}
{{--                --}}{{--                    <div class="menu_list">--}}
{{--                --}}{{--                        <a href="#">Казахстан</a>--}}
{{--                --}}{{--                        <a href="#">ОАЭ</a>--}}
{{--                --}}{{--                        <a href="#">Турция</a>--}}
{{--                --}}{{--                        <a href="#">Грузия</a>--}}
{{--                --}}{{--                        <a href="#">Италия</a>--}}
{{--                --}}{{--                        <a href="#">Египет</a>--}}
{{--                --}}{{--                        <a href="#" class="menu_block_btn">--}}
{{--                --}}{{--                            Все страны--}}
{{--                --}}{{--                        </a>--}}
{{--                --}}{{--                    </div>--}}
{{--                --}}{{--                </div>--}}
{{--                --}}{{--                <div class="menu_block">--}}
{{--                --}}{{--                    <h4>{{ __('messages.cities') }}</h4>--}}
{{--                --}}{{--                    <div class="menu_list">--}}
{{--                --}}{{--                        <a href="#">Астана</a>--}}
{{--                --}}{{--                        <a href="#">Ташкент</a>--}}
{{--                --}}{{--                        <a href="#">Нурсултан</a>--}}
{{--                --}}{{--                        <a href="#">Шымкент</a>--}}
{{--                --}}{{--                        <a href="#">Самарканд</a>--}}
{{--                --}}{{--                        <a href="#">Бухара</a>--}}
{{--                --}}{{--                        <a href="#" class="menu_block_btn">--}}
{{--                --}}{{--                            {{ __('messages.all_cities') }}--}}
{{--                --}}{{--                        </a>--}}
{{--                --}}{{--                    </div>--}}
{{--                --}}{{--                </div>--}}
{{--                --}}{{--                <div class="menu_block">--}}
{{--                --}}{{--                    <h4>{{ __('messages.aviacompanies') }}</h4>--}}
{{--                --}}{{--                    <div class="menu_list">--}}
{{--                --}}{{--                        <a href="#">Turkish airlanes</a>--}}
{{--                --}}{{--                        <a href="#">Uzbekistan airways</a>--}}
{{--                --}}{{--                        <a href="#">Air France</a>--}}
{{--                --}}{{--                        <a href="#">Scat</a>--}}
{{--                --}}{{--                        <a href="#">Back Air</a>--}}
{{--                --}}{{--                        <a href="#" class="menu_block_btn">--}}
{{--                --}}{{--                            {{ __('messages.all_aviacompanies') }}--}}
{{--                --}}{{--                        </a>--}}
{{--                --}}{{--                    </div>--}}
{{--                --}}{{--                </div>--}}
{{--                --}}{{--                <div class="menu_block">--}}
{{--                --}}{{--                    <h4>{{ __('messages.airports') }}</h4>--}}
{{--                --}}{{--                    <div class="menu_list">--}}
{{--                --}}{{--                        <a href="#">Астана</a>--}}
{{--                --}}{{--                        <a href="#">Ташкент</a>--}}
{{--                --}}{{--                        <a href="#">Нурсултан</a>--}}
{{--                --}}{{--                        <a href="#">Шымкент</a>--}}
{{--                --}}{{--                        <a href="#">Самарканд</a>--}}
{{--                --}}{{--                        <a href="#">Бухара</a>--}}
{{--                --}}{{--                        <a href="#" class="menu_block_btn">--}}
{{--                --}}{{--                            {{ __('messages.all_airports') }}--}}
{{--                --}}{{--                        </a>--}}
{{--                --}}{{--                    </div>--}}
{{--                --}}{{--                </div>--}}
{{--                --}}{{--                <div class="menu_block">--}}
{{--                --}}{{--                    <h4>{{ __('messages.directions') }}</h4>--}}
{{--                --}}{{--                    <div class="menu_list">--}}
{{--                --}}{{--                        <a href="#">Узбекистан - Казахстан</a>--}}
{{--                --}}{{--                        <a href="#">ОАЭ - Астана</a>--}}
{{--                --}}{{--                        <a href="#">Турция - Россия</a>--}}
{{--                --}}{{--                        <a href="#">Грузия - Италия</a>--}}
{{--                --}}{{--                        <a href="#">Италия - Испания</a>--}}
{{--                --}}{{--                        <a href="#">Египет - Турция</a>--}}
{{--                --}}{{--                        <a href="#" class="menu_block_btn">--}}
{{--                --}}{{--                            {{ __('messages.all_directions') }}--}}
{{--                --}}{{--                        </a>--}}
{{--                --}}{{--                    </div>--}}
{{--                --}}{{--                </div>--}}
{{--                --}}{{--                <div class="menu_block">--}}
{{--                --}}{{--                    <h4>{{ __('messages.products') }}--}}
{{--                --}}{{--                    </h4>--}}
{{--                --}}{{--                    <div class="menu_list">--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.airplane_tickets') }}</a>--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.railway_tickets') }}</a>--}}
{{--                --}}{{--                    </div>--}}
{{--                --}}{{--                </div>--}}
{{--                --}}{{--                <div class="menu_block">--}}
{{--                --}}{{--                    <h4>                            {{ __('messages.for_users') }}--}}
{{--                --}}{{--                    </h4>--}}
{{--                --}}{{--                    <div class="menu_list">--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.bonus') }}</a>--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.faq') }}</a>--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.reviews') }}</a>--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.best_price') }}</a>--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.race_register') }}</a>--}}
{{--                --}}{{--                    </div>--}}
{{--                --}}{{--                </div>--}}
{{--                --}}{{--                <div class="menu_block">--}}
{{--                --}}{{--                    <h4>{{ __('messages.for_partners') }}--}}
{{--                --}}{{--                    </h4>--}}
{{--                --}}{{--                    <div class="menu_list">--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.corp_clients') }}</a>--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.advertising') }}</a>--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.partnership') }}</a>--}}
{{--                --}}{{--                        <a href="#">{{ __('messages.agent') }}</a>--}}
{{--                --}}{{--                    </div>--}}
{{--                --}}{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    --}}{{--    <div class="container">--}}
{{--    --}}{{--        <div class="row second_row">--}}
{{--    --}}{{--            <div class="footer_apps">--}}
{{--    --}}{{--                <a href="#">--}}
{{--    --}}{{--                    <img src="{{ asset('img/google.png') }}">--}}
{{--    --}}{{--                </a>--}}
{{--    --}}{{--                <a href="#">--}}
{{--    --}}{{--                    <img src="{{ asset('img/appstore.png') }}">--}}
{{--    --}}{{--                </a>--}}
{{--    --}}{{--            </div>--}}
{{--    --}}{{--        </div>--}}
{{--    --}}{{--    </div>--}}
{{--    --}}{{--</footer>--}}
{{--    --}}{{--<script src="{{ asset('js/libs.min.js') }}"></script>--}}
{{--    --}}{{--<script src="{{ asset('js/script.js') }}"></script>--}}
{{--    --}}{{--<script type="text/javascript">--}}
{{--    --}}{{--    $('#beginning').datepicker({--}}
{{--    --}}{{--        format: 'd M, yyyy',--}}
{{--    --}}{{--        orientation: 'top',--}}
{{--    --}}{{--        autoclose: true--}}
{{--    --}}{{--    });--}}
{{--    --}}{{--    $('#end').datepicker({--}}
{{--    --}}{{--        format: 'd M, yyyy',--}}
{{--    --}}{{--        orientation: 'top',--}}
{{--    --}}{{--        autoclose: true--}}
{{--    --}}{{--    });--}}
{{--    --}}{{--</script>--}}
{{--    --}}{{--</body>--}}
{{--    --}}{{--</html>--}}

    <!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.jpg') }}">
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="yandex-verification" content="b00dcd5ed9648cc2" />
<!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.js') }}"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/libs.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/media.css') }}">
    <title>Lokoplane</title>
</head>
<body class="x_hidden">
<div class="modal_overlay"></div>
<a href="{{route('taplink')}}">
    <div class="call_btn">
        <img src="{{ asset('img/call.svg') }}">
    </div>
</a>
<header class="main_header">
    <div class="row">
        <a href="#" class="logo_block">
            <h3 class="logo_text"><span>loko</span>plane</h3>
        </a>
        @auth
            <div class="user_block"><a href="/cabinet"><img src="{{ asset('img/user.svg') }}" class="user_icon"></a></div>
        @endauth
        @guest
            <div class="user_block">
                <img src="{{ asset('img/user.svg') }}" class="user_icon">
                <div class="form_modal">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form_tab login_form active">
                            <div class="form_title">
                                <h4>Вход в кабинет</h4>
                                <img src="{{ asset('img/close.svg') }}" class="close">
                            </div>
                            <div class="form_content">
                                <div class="inputs_block">
                                    <div class="input_block">
                                        <p>email</p>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректный email') }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="input_block forgot_password">
                                        <p>пароль:</p>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                        <img src="{{ asset('img/show.svg') }}" class="show">
                                    </div>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректный пароль') }}</strong>
                                        </span>
                                    @enderror
                                    @if (Route::has('password.request'))
                                        <a class="forgot_password_btn" href="{{ route('password.request') }}" style="padding-top: 12px;">
                                            {{ __('Востановить пароль?') }}
                                        </a>
                                    @endif
                                </div>
                                <div class="form_btns">
                                    <button type="submit" class="btn submit_btn">{{ __('Вход') }}</button>
                                    {{--                                <button class="btn fb_btn" type="button">--}}
                                    {{--                                    <img src="{{ asset('img/fb.svg') }}">--}}
                                    {{--                                    войти через Facebook--}}
                                    {{--                                </button>--}}
                                    {{--                                <button class="btn google_btn" type="button">--}}
                                    {{--                                    <img src="{{ asset('img/google.svg') }}">--}}
                                    {{--                                    войти через Google--}}
                                    {{--                                </button>--}}
                                    {{--                                <button class="btn vk_btn" type="button">--}}
                                    {{--                                    <img src="{{ asset('img/vk.svg') }}">--}}
                                    {{--                                    войти через Vkontakte--}}
                                    {{--                                </button>--}}
                                </div>
                                <button type="button" class="change_tab_btn change_login">{{ __('Создать аккаунт') }}</button>
                            </div>
                        </div>
                    </form>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form_tab register_form">
                            <div class="form_title">
                                <h4>Создать аккаунт</h4>
                                <img src="{{ asset('img/close.svg') }}" class="close">
                            </div>
                            <div class="form_content">
                                <div class="input_block">
                                    <p>имя</p>
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректное имя') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="input_block">
                                    <p>email</p>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректный email') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <p class="password_msg">пароль должен состоять из не менее 8 символов</p>
                                <div class="input_block forgot_password">
                                    <p>пароль:</p>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректный пароль') }}</strong>
                                        </span>
                                    @enderror
                                    <img src="{{ asset('img/show.svg') }}" class="show">
                                </div>
                                <div class="input_block forgot_password">
                                    <p>повторите пароль:</p>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    <img src="{{ asset('img/show.svg') }}" class="show">
                                </div>
                                <div class="form_btns">
                                    <button type="submit" class="btn submit_btn">{{ __('создать') }}</button>
                                </div>
                                <button type="button" class="change_tab_btn change_register">Авторизоваться</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            {{--            <div class="menu_block">--}}
            {{--                <img src="{{ asset('img/main-menu.svg') }}">--}}
            {{--            </div>--}}
        @endguest
    </div>
</header>
<section class="main_section">
    <form action="/air-tickets/" class="banner_content row" method="get">
        <h2 class="section_title">Поиск выгодных авиабилетов</h2>
        <div class="toggler">
            <input type="radio" name="search_type" id="avia" value="avia" checked>
            <label for="avia" class="first" style="margin-right: -1px;">
                <img src="{{ asset('img/plane.svg') }}"><span>Авиабилеты</span>
            </label>
            <input type="radio" name="search_type" id="railway" value="railway">
        </div>
        <div class="input_row">
            <button type="button" class="additional_btn tram_btn">расписание</button>
            <div class="banner_input dropdown_input where_from">
                <img src="{{ asset('img/exchange.svg') }}" class="exchange hide_md">
                <img src="{{ asset('img/exchange_mobile.svg') }}" class="exchange show_md">
                <p>откуда</p>
                <div class="dropdown_title">
                    <input type="text" class="search_input city_input" value="Нур-Султан, Казахстан">
                    <span>{{ env('city_from_code') }}</span>
                </div>
                <div class="dropdown_block">
                    <div class="dropdown_list">
                        @foreach($cities as $key=>$city)
                            <div class="dropdown_item" data-input-value="{{$city}}"
                                 data-search-keyword="{{$city}}, {{$key}}">
                                <h4>{{$city}}</h4>
                                <p>{{$key}}</p>
                            </div>
                        @endforeach
                    </div>
                    <input type="hidden" class="hidden_input">
                    <input name="from_input" type="hidden" class="hidden_input from_to_input" value="{{ env('city_from_code') }}">
                </div>
            </div>
            <div class="banner_input dropdown_input where_from where_input">
                <p>куда</p>
                <div class="dropdown_title">
                    <input type="text" class="search_input city_input" value="Алматы, Казахстан">
                    <span>{{ env('city_to_code') }}</span>
                </div>
                <div class="dropdown_block">
                    <div class="dropdown_list">
                        @foreach($cities as $key=>$city)
                            <div class="dropdown_item" data-input-value="{{$city}}"
                                 data-search-keyword="{{$city}}, {{$key}}">
                                <h4>{{$city}}</h4>
                                <p>{{$key}}</p>
                            </div>
                        @endforeach
                    </div>
                    <input type="hidden" class="hidden_input">
                    <input name="to_input" type="hidden" class="hidden_input from_to_input" value="{{ env('city_to_code') }}">
                </div>
            </div>
            <div class="banner_input start date">
                <p>дата туда</p>
                <input name="beginning" type="text" id="beginning" value="{{$beginning}}">
                <img src="{{ asset('img/calendar.svg') }}" class="icon">
            </div>
            <div class="banner_input date finish_block" style="display: none;">
                <p>дата обратно</p>
                <img src="{{ asset('img/calendar.svg') }}" class="icon">
                <input name="end" type="text" id="end">
            </div>
            <div class="banner_input dropdown_input types_dropdown">
                <p>Количество людей</p>
                <div class="dropdown_title">
                    <span data-text="adults">1 </span> взр,
                    <span data-text="child">0 </span> дет,
                    <span data-text="baby">0 </span> млад,
                    <span class="type trip_type last">Эконом</span>
                </div>
                <div class="dropdown_block">
                    <div class="dropdown_content">
                        <div class="item">
                            <div class="item_text">
                                <h4>Взрослые</h4>
                                <p>старше 12 лет</p>
                            </div>
                            <div class="amount">
                                <button type="button" class="minus" data-text-output="adults">-</button>
                                <input name="adults" type="text" value="1" readonly>
                                <button type="button" class="plus" data-text-output="adults">+</button>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_text">
                                <h4>Дети</h4>
                                <p>младше 12 лет</p>
                            </div>
                            <div class="amount">
                                <button type="button" class="minus" data-text-output="child">-</button>
                                <input name="child" type="text" value="0" readonly>
                                <button type="button" class="plus" data-text-output="child">+</button>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item_text">
                                <h4>Младенцы</h4>
                                <p>младше 2 лет</p>
                            </div>
                            <div class="amount">
                                <button type="button" class="minus" data-text-output="baby">-</button>
                                <input name="baby" type="text" value="0" readonly>
                                <button type="button" class="plus" data-text-output="baby">+</button>
                            </div>
                        </div>
                    </div>
                    <div class="radio_btns">
                        <div class="radio_item">
                            <input name="price_type" value="E" type="radio" id="radio1" checked>
                            <label for="radio1">{{ __('messages.econom') }}</label>
                        </div>
                        <div class="radio_item">
                            <input name="price_type" value="W" type="radio" id="radio2">
                            <label for="radio2">{{ __('messages.business_class') }}</label>
                        </div>
                        <div class="radio_item">
                            <input name="price_type" value="B" type="radio" id="radio3">
                            <label for="radio3">{{ __('messages.first') }}</label>
                        </div>
                        <div class="radio_item">
                            <input name="price_type" value="F" type="radio" id="radio4">
                            <label for="radio4">{{ __('messages.search_all') }}</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="radios_block">
            <div class="radio_item">
                <input type="radio" id="radio5" name="type" class="two_side">
                <label for="radio5">Туда-обратно</label>
            </div>
            <div class="radio_item">
                <input type="radio" id="radio6" name="type" class="one_side" checked>
                <label for="radio6">В одну сторону</label>
            </div>
        </div>
        <button type="submit" class="submit_btn">
            <div class="plane_btn">
                Полетели!
                <img src="{{ asset('img/plane.svg') }}">
            </div>
            <div class="railway_btn">
                Поехали!
                <img src="{{ asset('img/tram2.svg') }}">
            </div>
        </button>
    </form>
    <div class="clouds_block">
        <div class="cloud_item first lax" data-lax-translate-y="0 0, 300 -50">
            <img src="{{ asset('img/p3.svg') }}" class="plane p3 lax" data-lax-translate-x="0 0, 300 -50">
            <img src="{{ asset('img/p5.svg') }}" class="plane p5 lax" data-lax-rotate-y="0 0, 300 -50">
            <img src="{{ asset('img/clouds3.svg') }}" class="cloud_img lax" data-lax-translate-x="0 0, 300 -50"
                 data-lax-scale="0 1, 400 1.15">
        </div>
        <div class="cloud_item second lax" data-lax-translate-y="0 0, 300 -50">
            <img src="{{ asset('img/p2.svg') }}" class="plane p2 lax" data-lax-translate-x="0 0, 300 -150"
                 data-lax-rotate-x="0 0, 300 70">
            <img src="{{ asset('img/clouds2.svg') }}" class="cloud_img lax" data-lax-translate-x="0 0, 300 50"
                 data-lax-scale="0 1, 400 1.15">
        </div>
        <div class="cloud_item third lax" data-lax-translate-y="0 0, 300 -50">
            <img src="{{ asset('img/p1.svg') }}" class="plane p1 lax" data-lax-translate-y="0 0, 300 50"
                 data-lax-scale="0 1, 300 0.8" data-lax-rotate-x="0 0, 300 -50">
            <img src="{{ asset('img/p4.svg') }}" class="plane p4 lax" data-lax-translate-x="0 0, 300 50"
                 data-lax-translate-y="0 0, 300 50" data-lax-rotate-y="0 0, 300 -10">
            <img src="{{ asset('img/p6.svg') }}" class="plane p6 lax" data-lax-translate-x="0 0, 300 -50"
                 data-lax-scale="0 1, 300 1.1">
            <img src="{{ asset('img/clouds1.svg') }}" class="cloud_img lax" data-lax-scale="0 1, 400 1.05">
        </div>
    </div>
    <img src="{{ asset('img/visuals.png') }}" class="cloud railway">
    <img src="{{ asset('img/railway_bg_mobile.svg') }}" class="shape_mobile railway">
    <img src="{{ asset('img/plane_bg_mobile.svg') }}" class="shape_mobile plane_shape">
</section>
<section class="cards_section lax" data-lax-scale="0 0.4, 500 1">
    <div class="container">
        <h2 class="section_title">Программа <br>лояльности <br>LOCOBONUS</h2>
        <div class="row">
            <div class="card">
                <img src="{{ asset('img/ultra.png') }}">
            </div>
            <div class="card">
                <img src="{{ asset('img/gold.png') }}">
            </div>
            <div class="card">
                <img src="{{ asset('img/silver.png') }}">
            </div>
        </div>
    </div>
</section>
<footer class="main_footer">
    <div class="first_footer">
        <div class="container">
            <div class="main_footer_menu row">
                <div class="menu_block" style="text-align: right; width: 50%; margin-right: 5px;">
                    <h4 style="display: block;"><img src="{{ asset('img/expand.svg') }}">Пользователям</h4>
                    <div class="menu_list">
                        <a href="{{route('post', 'oferta')}}">Публичная оферта</a>
                        <a href="{{route('post', 'rules')}}">Правила перелета</a>
                    </div>
                </div>
                <div class="menu_block" style="text-align: left; width: 50%; margin-left: 5px;">
                    <h4>Партнерам<img src="{{ asset('img/expand.svg') }}"></h4>
                    <div class="menu_list">
                        <a href="{{route('corplanding')}}">Корпоративным клиентам</a>
                        <a href="{{route('post', 'privacy')}}">Политика конфиденциальности</a>
                        <!--<a href="{{route('taplink')}}">Рекламодателям</a>!-->
                        <!--<a href="{{route('taplink')}}">Сотрудничество</a>!-->
                        {{--                            <a href="{{route('corplanding')}}">Стать агентом</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row second_row">
            <div class="footer_apps">
                <a href="#">
                    <img src="{{ asset('img/google.png') }}">
                </a>
                <a href="#">
                    <img src="{{ asset('img/appstore.png') }}">
                </a>
            </div>
        </div>
    </div>
</footer>

<script src="js/libs.min.js"></script>
<script src="js/script.js"></script>
<script src="js/rallax.js"></script>
<script type="text/javascript">
    $('#beginning').datepicker({
        'format': 'yyyy-mm-dd',
        'orientation': 'top',
        'autoclose': true
    });
    $('#end').datepicker({
        'format': 'yyyy-mm-dd',
        'orientation': 'top',
        'autoclose': true
    });
    // $('#beginning').datepicker({
    //     format: 'd M, yyyy',
    //     orientation: 'top',
    //     autoclose: true
    // });
    // $('#end').datepicker({
    //     format: 'd M, yyyy',
    //     orientation: 'top',
    //     autoclose: true
    // });

    lax.setup()

    // update every scroll
    const updateLax = () => {
        lax.update(window.scrollY)
        window.requestAnimationFrame(updateLax)
    }

    window.requestAnimationFrame(updateLax)

</script>
