@extends('layouts.app')

@section('content')
<section class="main_section text_section">
    <h2 class="section_title">{{$post->title}}</h2>
    <a href="/"><img src="/img/arrow.svg">Вернуться на главную</a>
</section>
<section class="article_section">
    <div class="container">
        <div class="row">{!! $post->description !!}</div>
    </div>
</section>

@endsection
