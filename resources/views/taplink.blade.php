<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="/img/favicon.jpg">
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="/css/taplink-style.css">
    <title>Lokoplane</title>

</head>
<body>
<section class="taplink">
    <div class="taplink_container">
        <a href="/" class="logo">
            <img src="/img/taplink/loko_logo.svg">
        </a>
        <div class="taplink_row">
            <a href="/" class="item">
                <div class="icon">
                    <img src="/img/taplink/flight.svg">
                </div>
                <h4>Купить билет <br>на самолет</h4>
            </a>
            <a href="https://t.me/lokoplane_bot"  class="item">
                <div class="icon">
                    <img src="/img/taplink/calendar.svg">
                </div>
                <h4>Изменить <br>дату вылета</h4>
            </a>
            <a href="https://t.me/lokoplane_bot"  class="item">
                <div class="icon">
                    <img src="/img/taplink/tickets.svg">
                </div>
                <h4>Возврат <br>билета</h4>
            </a>
            <a href="/corp-landing" class="item">
                <div class="icon">
                    <img src="/img/taplink/hat.svg">
                </div>
                <h4>Корпоративное <br>обслуживание</h4>
            </a>
            <a href="whatsapp://+77781539927" class="item">
                <div class="icon">
                    <img src="/img/taplink/whatsapp.svg">
                </div>
                <h4>Служба <br>поддержки <br>пассажиров</h4>
            </a>
            <a href="https://t.me/lokoplane_bot" class="item">
                <div class="icon">
                    <img src="/img/taplink/telegram.svg">
                </div>
                <h4>Служба <br>поддержки <br>пассажиров</h4>
            </a>
        </div>
        <div class="lokobonus">
            <h4>Программа лояльности</h4>
            <a href="#" class="button">
                <img src="/img/taplink/loko.svg">
                LOKOBONUS
            </a>
            </a>
        </div>
</section>
</body>
</html>
