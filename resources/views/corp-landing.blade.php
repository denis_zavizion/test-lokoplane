<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="/img/favicon.jpg">
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="/css/corp-style.css">
    <title>Lokoplane</title>

</head>
<body>
<div class="main_container">
    <header>
        <div class="row">
            <a href="/">
                <img src="/img/corp/logo.svg">
            </a>
            <div class="socials">
                <a href="https://www.instagram.com/lokoplane.kz/">
                    <img src="/img/corp/insta.svg">
                    <span>@lokoplane.kz</span>
                </a>
                <a href="https://t.me/lokoplane_bot">
                    <img src="/img/corp/telegram.svg">
                    <span>@lokoplane_bot</span>
                </a>
            </div>
        </div>
    </header>
    <section class="banner">
        <img src="/img/corp/corp_bg.svg" class="bg">
        <div class="row">
            <h2>Удобный способ покупать<br>
                недорогие билеты<br>
                <span>на самолет и поезд<br>
                в #lokoplane</span>
            </h2>
        </div>
    </section>
    <section class="features">
        <h2 class="section_title">Предлагаем вашей компании</h2>
        <div class="row">
            <div class="feature_item">
                <img src="/img/corp/passport.png">
                <h4>Билеты от 320 авиакомпаний<br>Среди них Emirates, Air <br>Astana,
                    American Airlines, <br>Turkish Airlines, Lufthansa, <br>KLM, Аэрофлот, SCAT.</h4>
            </div>
            <div class="feature_item">
                <img src="/img/corp/headphone.png">
                <h4>Приоритетную линию <br>в колл-центре 24/7.<br>
                    Сотрудники говорят<br>
                    на 3 языках: казахском, <br>русском и английском</h4>
            </div>
            <div class="feature_item">
                <img src="/img/corp/calculator.png">
                <h4>Закрывающие документы -<br>
                    полный комплект <br>документов для <br>бухгалтерского отчета</h4>
            </div>
            <div class="feature_item">
                <img src="/img/corp/suitcase.png">
                <h4>В случае утери багажа, <br>наши партнеры <br>возместят вам <br>приченный ущерб</h4>
            </div>
            <div class="feature_item">
                <img src="/img/corp/wallet.png">
                <h4>Мощный cash back при <br>групповых перелетах.<br>
                    Сплоченная команда <br>всегда в плюсе</h4>
            </div>
            <div class="feature_item">
                <img src="/img/corp/card.png">
                <h4>Удобные способы оплаты - <br>наличными курьеру, <br>банковским переводом</h4>
            </div>
        </div>
    </section>
    <section class="cabinet">
        <div class="cabinet_title_block">
            <img src="/img/corp/title_bg.svg">
            <div class="cabinet_title">
                <h2>Личный кабинет организации -</h2>
                <h3>гибкий способ управлять всеми перелетами</h3>
            </div>

        </div>
        <div class="cabinet_content">
            <img src="/img/corp/example.png" class="example">
            <h4 class="cabinet_text">
                Мы продаем билеты только на проверенные временем авиакомпании.
                Мы не выписываем билеты на рейсы авиакомпаний, которые
                не считаем безопасными.<br>
                Для нас важна безопасность каждого пассажира.
            </h4>
        </div>
    </section>
    <footer>
        <h2>Для бронирования билетов напишите нам на почту
            corporate@lokoplane.kz<br><br>

            либо позвоните по телефону CALL CENTER
            +7 778 153 99 27<br><br>

            www.lokoplane.kz</h2>
        <img src="/img/corp/bg.png" class="footer_bg">
    </footer>
</div>


</body>
</html>
