@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Авторизация</div>
{{--                    <div class="card-body">--}}
{{--                        <form method="POST" action="{{ route('login') }}">--}}
{{--                            @csrf--}}
{{--                            <div class="form-group row">--}}
{{--                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}
{{--                                    @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="form-group row">--}}
{{--                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}
{{--                                    @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group row">--}}
{{--                                <div class="col-md-6 offset-md-4">--}}
{{--                                    <div class="form-check">--}}
{{--                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                        <label class="form-check-label" for="remember">--}}
{{--                                            {{ __('Запомнить меня') }}--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group row mb-0">--}}
{{--                                <div class="col-md-8 offset-md-4">--}}
{{--                                    <button type="submit" class="btn btn-primary">--}}
{{--                                        {{ __('Вход') }}--}}
{{--                                    </button>--}}

{{--                                    @if (Route::has('password.request'))--}}
{{--                                        <a class="btn btn-link" href="{{ route('password.request') }}" style="padding-top: 12px;">--}}
{{--                                            {{ __('Востановить пароль?') }}--}}
{{--                                        </a>--}}
{{--                                    @endif--}}
{{--                                    <div><a href="{{ url('/register') }}">Регистрация</a></div>--}}
{{--                                    <a href="{{ url('auth/google') }}" style="margin-top: 20px;" class="btn btn-lg btn-success btn-block">--}}
{{--                                        <strong>Login With Google</strong>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}

                    <div class="user_block">
                        <div class="form_modal" style="display: block; position: unset; width: 100%">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form_tab login_form active">
                                    <div class="form_title">
                                        <h4>Вход в кабинет</h4>
{{--                                        <img src="{{ asset('img/close.svg') }}" class="close">--}}
                                    </div>
                                    <div class="form_content">
                                        <div class="inputs_block">
                                            <div class="input_block">
                                                <p>email</p>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректный email') }}</strong>
                                        </span>
                                                @enderror
                                            </div>
                                            <div class="input_block forgot_password">
                                                <p>пароль:</p>
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                                <img src="{{ asset('img/show.svg') }}" class="show">
                                            </div>
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректный пароль') }}</strong>
                                        </span>
                                            @enderror
                                            @if (Route::has('password.request'))
                                                <a class="forgot_password_btn" href="{{ route('password.request') }}" style="padding-top: 12px;">
                                                    {{ __('Востановить пароль?') }}
                                                </a>
                                            @endif
                                        </div>
                                        <div class="form_btns">
                                            <button type="submit" class="btn submit_btn">{{ __('Вход') }}</button>
                                        </div>
                                        <button type="button" class="change_tab_btn change_login">Создать аккаунт</button>
                                    </div>
                                </div>
                            </form>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form_tab register_form">
                                    <div class="form_title">
                                        <h4>Создать аккаунт</h4>
{{--                                        <img src="{{ asset('img/close.svg') }}" class="close">--}}
                                    </div>
                                    <div class="form_content">
                                        <div class="input_block">
                                            <p>имя</p>
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректное имя') }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                        <div class="input_block">
                                            <p>email</p>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректный email') }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                        <p class="password_msg">пароль должен состоять из не менее 8 символов</p>
                                        <div class="input_block forgot_password">
                                            <p>пароль:</p>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ __('Некорректный пароль') }}</strong>
                                        </span>
                                            @enderror
                                            <img src="{{ asset('img/show.svg') }}" class="show">
                                        </div>
                                        <div class="input_block forgot_password">
                                            <p>повторите пароль:</p>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                            <img src="{{ asset('img/show.svg') }}" class="show">
                                        </div>
                                        <div class="form_btns">
                                            <button type="submit" class="btn submit_btn">{{ __('создать') }}</button>
                                        </div>
                                        <button type="button" class="change_tab_btn change_register">Авторизоваться</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
