@extends('layouts.app')
@section('content')

<script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
<section class="inner_section cabinet_section">
    <div class="row">
        <div class="main_block width_100_md" style="width: 100%;">
            <div class="order_form_block" id="block_result_loading">
                @if(isset($order->status) && $order->status == 'ok' && isset($order->data) && $order->data)
                    <div style="float: left;">
                        <h3>Проверьте данные и оплатите билет</h3>
                        @if(isset($order->data->status)) <div>Статус: {{$order->data->status}}</div> @endif
                        @if(isset($order->data->pnr_number) ) <div>Номер брони: {{$order->data->pnr_number}}</div> @endif
                        @if(isset($order->data->amount) && isset($order->data->currency)) <div>Сумма: {{$order->data->amount}} {{$order->data->currency}}</div> @endif
                        <br/>
                        @if(isset($order->data->buyer->email)) <div>Почта: {{$order->data->buyer->email}}</div> @endif
                        @if(isset($order->data->buyer->phone)) <div>Номер телефона: {{$order->data->buyer->phone}}</div> @endif
                        @if(isset($order->data->buyer->country->name)) <div>Страна: {{$order->data->buyer->country->name}}</div> @endif
                        @if(isset($order->data->stamp_create)) <div>Окончание брони : {{$order->data->stamp_create}}</div>  @endif
                    </div>
                    <div><img style="width: 40%; float: right;" src="/images/hotpng.png"></div>
                    @if(isset($status_buy) && $status_buy == 'active')
                        <input class="button_buy" type="button" value="Билет оплачен!">
                    @else
                        <input class="button_buy" type="button" id="buy_button_submit" value="Оплатить">
                    @endif

                @else
                    <div class="auth-error-block" style="display: block"><div style="font-size: 24px; text-align: center;">Бронирование не создано</div></div>
                @endif
            </div>
        </div>
    </div>
</section>


@if(isset($order->status) && $order->status == 'ok' && isset($order->data) && $order->data)
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
    $("#buy_button_submit").on( "click", function() {

        var widget = new cp.CloudPayments();
        widget.charge({ // options
                publicId: 'pk_22952aa3539e7feace109916b8656',  //id из личного кабинета
                description: 'Оплата за авиа билет', //назначение
                amount: {{$order->data->amount}}, //сумма
                currency: '{{$order->data->currency}}', //валюта
                invoiceId: '{{$order->data->order_id}}', //номер заказа  (необязательно) из БД
                accountId: '{{$order->data->buyer->email}}', //идентификатор плательщика (необязательно)  из БД
                skin: "mini", //дизайн виджета
                data: {
                    myProp: 'myProp value' //произвольный набор параметров
                }
            },
            function (options) { // success
                // alert('+');
                    var data = new Object();
                    data.amount = "{{$order->data->amount}}";
                    data.order_id = "{{$order->data->order_id}}";
                    $.ajax({
                        type: 'POST',
                        // url: '/air/tickets/payment',
                        url: '/payment',
                        timeout: 60000,
                        data: data,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            window.location.replace("/order/{{$order->data->order_id}}");
                            console.log(data);
                        },
                        error: function () {
                                // $('#block_result_loading').html('<div style="font-size: 24px; text-align: center;">Что то пошло не так</div>');
                        }
                    });
                //действие при успешной оплате
            },
            function (reason, options) { // fail
                // alert('-----------------------');
                // console.log('-----------------------');
                //действие при неуспешной оплате
            });
    });
</script>
@endif
@endsection
