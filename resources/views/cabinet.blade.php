@extends('layouts.cabinet')

@section('content')
    <section class="inner_section cabinet_section">
        <div class="row">
            <div class="left_block">
                <div class="user_info_block main_user_cabinet_block">
                    <div class="user_info">
                        <div class="user_ava">
                            @if(auth()->user()->id)
                                @if(Session::get('image'))
                                    <img src="/images/avatar/{{auth()->user()->id}}/{{ Session::get('image') }}">
                                @else
                                    @if($avatar)
                                        <img src="/images/avatar/{{auth()->user()->id}}/{{$avatar}}" id="avatar">
                                    @else
                                        <img src="img/johndoe.jpg" id="avatar">
                                    @endif
                                @endif
                            @else
                                <img src="img/johndoe.jpg" id="avatar">
                            @endif
                            <form action="{{ route('avatar.upload.post') }}" id="form_avatar_upload"
                                  enctype="multipart/form-data" method="POST">
                                @csrf
                                <div class="avatar_cabinet">
                                    <input type="file" accept="image/*" value="загрузить" id="avatar_upload" name="avatar">
                                </div>
                            </form>
                        </div>
                        <div class="user_name">
                            <h4>{{isset($users->first_name)&&$users->first_name ? $users->first_name: 'Имя'}} {{isset($users->last_name)&&$users->last_name ? $users->last_name: 'Фамилия'}}</h4>
                            <p class="first_p">#{{$users->id}}</p>
                        </div>
                    </div>
                    <div class="tabs">
                        <div class="tab_item active" data-tab-open="1">
                            Личный кабинет
                        </div>
                        <div class="tab_item" data-tab-open="2">
                            Мои билеты
                        </div>
                        <div class="tab_item" data-tab-open="3">
                            Неоплаченные брони
                        </div>
                        <a href="#modal_error" class="open_modal"></a><!-- ссылкa с href="#modal1", oткрoет oкнo с id = modal1-->
                    </div>
                </div>
            </div>

            <div class="main_block cabinet_main_block active_cabinet_tab" data-tab="1">
                <div class="block info_block">
                    <h3 class="block_title">Данные о пользователе</h3>
                    <div id="doc_1">
                        <div class="list_row">
                            <div class="list first_list">
                                <p><span>{{isset($users->first_name)&&$users->first_name ? $users->first_name: 'Имя'}}
                                        {{isset($users->last_name)&&$users->last_name ? $users->last_name: 'Фамилия'}}
                                        {{isset($users->middle_name)&&$users->middle_name ? $users->middle_name: 'Отчество'}}
                                    </span></p>
                                <p>Дата Рождения: <span> {{isset($users->birth_date)&&$users->birth_date ? $users->birth_date: 'Данных нет'}}</span></p>
                                <p>Гражданство: <span> {{isset($town)&&$town ? $town: 'Данных нет'}}</span></p>

                            </div>
                            <div class="list">
                                <div class="list_item"><img src="img/tick.svg"> Email: {{isset($users->email)&&$users->email ? $users->email: 'Данных нет'}}</div>
                                <div class="list_item"><img src="img/tick.svg"> Телефон: {{isset($users->phone)&&$users->phone ? $users->phone: 'Данных нет'}}</div>
                            </div>
                        </div>
                    </div>

                    <form id="doc_form_1" class="order_input_row" method="post" action="{{ route('doc_cabinet_1') }}"
                          style="display: none">
                        @csrf
                        <div class="input_half_block">
                            <div class="input_half_block">
                                <div class="cabinet_input order_input">
                                    <p>Ваша фамилия</p>
                                    <input name="last_name" type="text" placeholder="Ivanov"
                                           value="{{$users->last_name}}">
                                </div>
                                <div class="cabinet_input order_input">
                                    <p>Ваше имя</p>
                                    <input name="first_name" type="text" placeholder="Ivan"
                                           value="{{$users->first_name}}">
                                </div>
                                <div class="cabinet_input order_input name_input">
                                    <p>Отчество</p>
                                    <input name="middle_name" type="text" placeholder="Ivanovich"
                                           value="{{$users->middle_name}}">
                                </div>
                                <div class="cabinet_input order_input">
                                    <p>Дата рождения</p>
                                    <input name="birth_date" type="text" class="form_date" placeholder="дд.мм.гггг"
                                           value="{{$users->birth_date}}">
                                </div>
                                <div class="cabinet_input order_input name_input">
                                    <p>Номер телефона</p>
                                    <input name="phone" type="text" placeholder="77777" value="{{$users->phone}}">
                                </div>
                                <div class="cabinet_input order_input">
                                    <p>Почта</p>
                                    <input name="mail" type="text" placeholder="@mail.com" value="{{$users->email}}">
                                </div>
                            </div>
                            <div class="dropdown_input order_input more_z4">
                                <p>Гражданство</p>
                                <div class="cabinet_input dropdown_title">
                                    <input type="text" class="search_input city_input" value="{{$town}}">
                                </div>
                                <div class="dropdown_block">
                                    <div class="dropdown_list">
                                        @include('city')
                                    </div>
                                    <input name="citizenship" type="hidden" class="hidden_input" value="{{$city_code}}">
                                </div>
                            </div>
                        </div>
                        <input id="doc_save_1" type="submit" class="block_btn" value="SAVE"
                               style="color: #fff; display: none">
                    </form>
                    <a id="doc_edit_1" href="#" class="block_btn"><img src="img/pen.svg"></a>
                </div>

                <div class="block info_block">
                    <h3 class="block_title">Документы</h3>
                    <div id="doc_2">
                        <div class="list_row">
                            <div class="list first_list">
                                <p>Документ: <span>{{isset($passport)&&$passport ? $passport: 'Данных нет'}}</span></p>
                                <p>Номер документа: <span>{{isset($users->number_document)&&$users->number_document ? $users->number_document: 'Данных нет'}}</span></p>
                                <p>Дата окончания срока действия паспорта: <span>{{isset($users->expire)&&$users->expire ? $users->expire: 'Данных нет'}}</span></p>
                            </div>
                        </div>
                    </div>
                    <form id="doc_form_2" class="order_input_row" method="post" action="{{ route('doc_cabinet_2') }}" style="display: none">
                        @csrf
                        <div class="dropdown_input order_input more_z4">
                            <p>Документ</p>
                            <div class="cabinet_input dropdown_title">
                                <input type="text" class="search_input city_input" value="{{$passport}}">
                            </div>
                            <div class="dropdown_block">
                                <div class="dropdown_list">
                                    <div class="dropdown_item" data-input-value="NI" data-search-keyword="Бельгия"><h4>Удостоверение личности KAZ</h4></div>
                                    <div class="dropdown_item" data-input-value="PSP" data-search-keyword=""><h4>Заграничный паспорт</h4></div>
                                    <div class="dropdown_item" data-input-value="PS" data-search-keyword=""><h4>Паспорт внутренний</h4></div>
                                    <div class="dropdown_item" data-input-value="NP" data-search-keyword=""><h4>Национальный паспорт</h4></div>
                                    <div class="dropdown_item" data-input-value="SR" data-search-keyword=""><h4>Свидетельство о рождении</h4></div>
                                    <div class="dropdown_item" data-input-value="VB" data-search-keyword=""><h4>Военный билет</h4></div>
                                    <div class="dropdown_item" data-input-value="DP" data-search-keyword=""><h4>Дипломатический паспорт</h4></div>
                                    <div class="dropdown_item" data-input-value="SP" data-search-keyword=""><h4>Служебный паспорт</h4></div>
                                    <div class="dropdown_item" data-input-value="PM" data-search-keyword=""><h4>Паспорт моряка</h4></div>
                                    <div class="dropdown_item" data-input-value="UD" data-search-keyword=""><h4>Удостоверение депутата совета Федерации</h4></div>
                                    <div class="dropdown_item" data-input-value="UDL" data-search-keyword=""><h4>Удостоверение личности военнослужащего РФ</h4></div>
                                    <div class="dropdown_item" data-input-value="SPO" data-search-keyword=""><h4>Справка об освобождении из мест лишения свободы</h4></div>
                                    <div class="dropdown_item" data-input-value="VUL" data-search-keyword=""><h4>Удостоверение осужденного на выезд за пределы мест...</h4></div>
                                    <div class="dropdown_item" data-input-value="SPU" data-search-keyword=""><h4>Временное удостоверение личности</h4></div>
                                    <div class="dropdown_item" data-input-value="VV" data-search-keyword=""><h4>Вид на жительство</h4></div>
                                    <div class="dropdown_item" data-input-value="CVV" data-search-keyword=""><h4>Свидетельство на возвращение в страны СНГ</h4></div>
                                </div>
                                <input name="type_document" type="hidden" class="hidden_input" value="{{$passport_code}}">
                            </div>
                        </div>
                        <div class="order_input">
                            <p>Номер документа</p>
                            <div class="cabinet_input dropdown_title">
                                <input name="number_document" id="number_document" type="text" placeholder=""
                                       value="{{$users->number_document}}">
                            </div>
                        </div>
                        <div class="order_input">
                            <p>Дата окончания срока действия паспорта</p>
                            <div class="cabinet_input dropdown_title">
                                <input name="expire" id="expire" type="text" class="form_date" placeholder="дд.мм.гггг"
                                       value="{{$users->expire}}">
                            </div>
                        </div>
                        {{--                        <div class="dropdown_input order_input more_z5">--}}
                        {{--                            <p>Документ</p>--}}
                        {{--                            <div class="cabinet_input dropdown_title">--}}
                        {{--                                <input type="text" class="search_input city_input">--}}
                        {{--                            </div>--}}
                        {{--                            <div class="dropdown_block">--}}
                        {{--                                <div class="dropdown_list">--}}
                        {{--                                    --}}
                        {{--                                </div>--}}
                        {{--                                <input name="Amadeus" type="hidden" class="hidden_input">--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <input id="doc_save_2" type="submit" class="block_btn" value="SAVE"
                               style="color: #fff; display: none">
                    </form>
                    <a id="doc_edit_2" href="#" class="block_btn"><img src="img/pen.svg"></a>


                </div>
                <div class="block cards_block">
                    <h3 class="block_title">Способ оплаты</h3>
                    <div class="cards_row">
                        <div class="card_item">
                            <img src="img/visa.png">
                        </div>
                        <div class="card_item">
                            <img src="img/master_card.png">
                        </div>
                        <div class="card_item">
                            <img src="img/add_card.png">
                        </div>
                    </div>
                    <a href="#" class="block_btn">
                        <img src="img/pen.svg">
                    </a>
                </div>
            </div>
            <div class="main_block cabinet_main_block" data-tab="2">
                <div class="block trips_block">
                    @foreach($tickets as $ticket)
                        <div class="left_block result_body width_100_md" id="app" style="width: 100%">
                            <div class="air_item mobile_air_item">
                                <div class="air_price">
                                    <h4>{{json_decode($ticket->json)->price}} тг</h4>
                                    <p>цена
                                        за @if(isset(json_decode($ticket->json)->price_details[0]->qty)){{json_decode($ticket->json)->price_details[0]->qty}} @endif
                                        пассажира</p>
                                </div>
                                <div class="air_desc">
                                    <div class="desc_row">
                                        <div class="start air_date">
                                            <h4>{{json_decode($ticket->json)->departure_time}}</h4>
                                            <h5>{{json_decode($ticket->directions)[0]->departure_name}}</h5>
                                        </div>
                                        <div class="route_info">
                                            <p class="first_route_date">{{json_decode($ticket->json)->departure_date}}</p>
                                            <div class="route_row">
                                                @foreach(json_decode($ticket->json)->flights_info as $segm)
                                                    <h4>{{$segm->departure_airport}}</h4>
                                                @endforeach
                                                @if(json_decode($ticket->json)->arrival_airport)
                                                    <h4>{{$segm->arrival_airport}}</h4>
                                                @endif
                                            </div>
                                            <p class="second_route_date">{{json_decode($ticket->json)->arrival_date}}</p>
                                        </div>
                                        <div class="end_air_date air_date">
                                            <h4>{{json_decode($ticket->json)->arrival_time}}</h4>
                                            <h5>{{json_decode($ticket->directions)[0]->arrival_name}}</h5>
                                        </div>
                                    </div>

                                    <div class="more_route_info" style="display: block;">
                                        <div class="flex_row">
                                            <div class="route_desc_column">
                                                @foreach(json_decode($ticket->json)->flights_info as $k_y=>$segm)
                                                    @if($k_y > 0)
                                                        <p class="note">
                                                            пересадка {{json_decode($ticket->json)->flights_info[$k_y-1]->stop_time}}
                                                            в {{json_decode($ticket->json)->flights_info[$k_y-1]->departure_city}}</p>
                                                    @endif
                                                    <h4 class="route_item_mobile">{{json_decode($ticket->json)->flights_info[$k_y]->departure_local_time}}</h4>
                                                    <div class="route_item">
                                                        <div class="route_left">
                                                            <h4>{{$segm->departure_local_time}} {{$segm->departure_airport}} {{$segm->departure_city}}</h4>
                                                            <h4>{{$segm->arrival_local_time}} {{$segm->arrival_airport}} {{$segm->arrival_city}}</h4>
                                                        </div>
                                                        <div class="route_right">
                                                            <img src="{{$segm->operating_airline_logo}}" class="logo">
                                                            <p>{{$segm->departure_city}}<br>
                                                                {{$segm->airplane_code}}<br>
                                                                {{$segm->airplane_name}}</p>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="main_block cabinet_main_block" data-tab="3">
                <div class="block trips_block">
                    @foreach($tickets_off as $ticket)
                        <div class="left_block result_body width_100_md" id="app" style="width: 100%">
                            <div class="air_item mobile_air_item">
                                <div class="air_price">
                                    <h4>{{json_decode($ticket->json)->price}} тг</h4>
                                    <p>цена
                                        за @if(isset(json_decode($ticket->json)->price_details[0]->qty)){{json_decode($ticket->json)->price_details[0]->qty}} @endif
                                        пассажира</p>
                                </div>
                                <div class="air_desc">
                                    <div class="desc_row">
                                        <div class="start air_date">
                                            <h4>{{json_decode($ticket->json)->departure_time}}</h4>
                                            <h5>{{json_decode($ticket->directions)[0]->departure_name}}</h5>
                                        </div>
                                        <div class="route_info">
                                            <p class="first_route_date">{{json_decode($ticket->json)->departure_date}}</p>
                                            <div class="route_row">
                                                @foreach(json_decode($ticket->json)->flights_info as $segm)
                                                    <h4>{{$segm->departure_airport}}</h4>
                                                @endforeach
                                                @if(json_decode($ticket->json)->arrival_airport)
                                                    <h4>{{$segm->arrival_airport}}</h4>
                                                @endif
                                            </div>
                                            <p class="second_route_date">{{json_decode($ticket->json)->arrival_date}}</p>
                                        </div>
                                        <div class="end_air_date air_date">
                                            <h4>{{json_decode($ticket->json)->arrival_time}}</h4>
                                            <h5>{{json_decode($ticket->directions)[0]->arrival_name}}</h5>
                                        </div>
                                    </div>
                                    <div class="more_route_info" style="display: block;">
                                        <div class="flex_row">
                                            <div class="route_desc_column">
                                                @foreach(json_decode($ticket->json)->flights_info as $k_y=>$segm)
                                                    @if($k_y > 0)
                                                        <p class="note">
                                                            пересадка {{json_decode($ticket->json)->flights_info[$k_y-1]->stop_time}}
                                                            в {{json_decode($ticket->json)->flights_info[$k_y-1]->departure_city}}</p>
                                                    @endif
                                                    <h4 class="route_item_mobile">{{json_decode($ticket->json)->flights_info[$k_y]->departure_local_time}}</h4>
                                                    <div class="route_item">
                                                        <div class="route_left">
                                                            <h4>{{$segm->departure_local_time}} {{$segm->departure_airport}} {{$segm->departure_city}}</h4>
                                                            <h4>{{$segm->arrival_local_time}} {{$segm->arrival_airport}} {{$segm->arrival_city}}</h4>
                                                        </div>
                                                        <div class="route_right">
                                                            <img src="{{$segm->operating_airline_logo}}" class="logo">
                                                            <p>{{$segm->departure_city}}<br>
                                                                {{$segm->airplane_code}}<br>
                                                                {{$segm->airplane_name}}</p>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="/order/{{$ticket->order_id}}" class="button_buy" style="width: 50%; margin: 20px auto 0 auto; font-size: 26px;">«оплатить»</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="main_block cabinet_main_block" data-tab="4">
                <div class="block info_block card_block">
                    <div class="card_title">
                        <h5>Ваш текущий пакет</h5>
                        <h2>Gold</h2>
                    </div>
                    <div class="card_row">
                        <img src="img/gold.png" class="card_img">
                        <div class="progress_bars_block">

                            {{--                            <div class="progress_row first">--}}
                            {{--                                <div class="progress_finish_text">--}}
                            {{--                                    <h4>Ultra</h4>--}}
                            {{--                                    <p>15 000 бонусов</p>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="progress_line">--}}
                            {{--                                    <div class="completed_line" style="width: 35%">--}}
                            {{--                                        <div class="end_line">--}}
                            {{--                                            <div class="end_text">--}}
                            {{--                                                <h4>Gold</h4>--}}
                            {{--                                                <p>8600 бонусов</p>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <div class="progress_row second">
                                <div class="progress_line">
                                    <div class="completed_line" style="left: 35%">
                                        <div class="end_line">
                                            <img src="img/plane_blue.svg">
                                            <div class="end_text">
                                                <h4>65</h4>
                                                <p>полетов</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="numbers">
                                    <h4>0</h4>
                                    <h4>50</h4>
                                    <h4>100</h4>
                                    <h4>150</h4>
                                    <h4>200</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card_footer">
                        <div class="card_info">
                            <div class="card_info_item">
                                <h4>Ваш бонусный счет</h4>
                                <h3>8600</h3>
                                <p>(6400 бонусов до следующего пакета)</p>
                            </div>
                            <div class="card_info_item">
                                <h4>Количество полетов</h4>
                                <h3>65</h3>
                            </div>
                        </div>
                        <div class="updated_info">
                            <p>Обновлено 28 авг 2019г</p>
                            <div class="icon">
                                <img src="img/refresh.svg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block info_block">
                    <h3 class="block_title">Действия с бонусами</h3>
                    <div class="types_row user_btns">
                        <a href="#">
                            <img src="img/cart.svg">
                            <p>купить бонусы</p>
                        </a>
                        <a href="#">
                            <img src="img/local.svg">
                            <p>потратить бонусы</p>
                        </a>
                        <a href="#">
                            <img src="img/gift.svg">
                            <p>перевести бонусы</p>
                        </a>
                    </div>
                    <a href="#" class="block_btn">
                        <img src="img/pen.svg">
                    </a>
                </div>
            </div>
            <div class="main_block cabinet_main_block" data-tab="5">
                <div class="block trips_block">
                    <h3 class="block_title">Добавить путишествия</h3>
                    <div class="trips_list">
                        <div class="trip_item">
                            <img src="img/plane_bg.png" class="watermark">
                            <h3>Нур-султан - Париж</h3>
                            <p>Рейс: КС-3625</p>
                            <div class="trip_list">
                                <h4><span>20 сен, ПТ 15:00,</span>Международный аэропорт Нурсултан Назарбаев</h4>
                                <h4><span>20 сен, СБ 18:00,</span>Шарн де-Голль</h4>
                            </div>
                        </div>
                        <div class="trip_item add_trip">
                            <h2>запланировать полет</h2>
                            <img src="img/trip_plane.png" class="watermark">
                        </div>
                        <div class="trip_item add_trip">
                            <h2>запланировать поездку</h2>
                            <img src="img/trip_tram.png" class="watermark">
                        </div>
                    </div>
                    <div class="trip_checkbox">
                        <input type="checkbox" id="checkbox1">
                        <label for="checkbox1">
                            <p class="type">напомнить о путишествии на email</p>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="inner_search header_search mb-0">
        <form action="/air-tickets/" method="get">
            <div class="row">
                <div class="input_row">
                    <button type="button" class="additional_btn tram_btn">{{ __('messages.timetable') }}</button>
                    <div class="toggler">
                        <input type="radio" name="search_type" id="avia" value="avia" checked>
                        <label for="avia" class="first">
                            <img src="{{ asset('img/plane.svg') }}">
                        </label>
                    </div>
                    <div class="banner_input dropdown_input where_from">
                        <img src="{{ asset('img/exchange.svg') }}" class="exchange hide_md">
                        <img src="{{ asset('img/exchange_mobile.svg') }}" class="exchange show_md">
                        <p>{{ __('messages.from') }}</p>
                        <div class="dropdown_title">
                            <input type="text" class="search_input city_input" value="Нур-Султан, Казахстан">
                            <span>{{ env('city_from_code') }}</span>
                        </div>
                        <div class="dropdown_block">
                            <div class="dropdown_list">
                                @foreach($cities as $key=>$city)
                                    <div class="dropdown_item" data-input-value="{{$city}}"
                                         data-search-keyword="{{$city}}, {{$key}}">
                                        <h4>{{$city}}</h4>
                                        <p>{{$key}}</p>
                                    </div>
                                @endforeach
                            </div>
                            <input type="hidden" class="hidden_input">
                            <input name="from_input" type="hidden" class="hidden_input from_to_input"
                                   value="{{ env('city_from_code') }}">
                        </div>
                    </div>
                    <div class="banner_input dropdown_input where_from where_input">
                        <p>{{ __('messages.where') }}</p>
                        <div class="dropdown_title">
                            <input type="text" class="search_input city_input" value="Алматы, Казахстан">
                            <span>{{ env('city_to_code') }}</span>
                        </div>
                        <div class="dropdown_block">
                            <div class="dropdown_list">
                                @foreach($cities as $key=>$city)
                                    <div class="dropdown_item" data-input-value="{{$city}}"
                                         data-search-keyword="{{$city}}, {{$key}}">
                                        <h4>{{$city}}</h4>
                                        <p>{{$key}}</p>
                                    </div>
                                @endforeach
                            </div>
                            <input type="hidden" class="hidden_input">
                            <input name="to_input" type="hidden" class="hidden_input from_to_input"
                                   value="{{ env('city_to_code') }}">
                        </div>
                    </div>
                    <div class="banner_input start date">
                        <p>дата</p>
                        <input name="beginning" type="text" id="beginning" value="{{$beginning}}">
                        <img src="{{ asset('img/calendar.svg') }}" class="icon">
                    </div>
                    <div class="banner_input dropdown_input types_dropdown">
                        <p>{{ __('messages.amount') }}</p>
                        <div class="dropdown_title">
                            <span data-text="adults">1 </span> {{ __('messages.adults_short') }},
                            <span data-text="child">0 </span> {{ __('messages.children_short') }},
                            <span data-text="baby">0 </span> {{ __('messages.babies_short') }},
                            <span class="type trip_type last">{{ __('messages.econom') }}</span>
                        </div>
                        <div class="dropdown_block">
                            <div class="dropdown_content">
                                <div class="item">
                                    <div class="item_text">
                                        <h4>{{ __('messages.adults') }}</h4>
                                        <p>{{ __('messages.older_12') }}</p>
                                    </div>
                                    <div class="amount">
                                        <button type="button" class="minus" data-text-output="adults">-</button>
                                        <input name="adults" type="text" value="1" readonly>
                                        <button type="button" class="plus" data-text-output="adults">+</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item_text">
                                        <h4>{{ __('Дети') }}</h4>
                                        <p>{{ __('messages.younger_12') }}</p>
                                    </div>
                                    <div class="amount">
                                        <button type="button" class="minus" data-text-output="child">-</button>
                                        <input name="child" type="text" value="0" readonly>
                                        <button type="button" class="plus" data-text-output="child">+</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item_text">
                                        <h4>{{ __('Младенцы') }}</h4>
                                        <p>{{ __('messages.younger_2') }}</p>
                                    </div>
                                    <div class="amount">
                                        <button type="button" class="minus" data-text-output="baby">-</button>
                                        <input name="baby" type="text" value="0" readonly>
                                        <button type="button" class="plus" data-text-output="baby">+</button>
                                    </div>
                                </div>
                            </div>
                            <div class="radio_btns">
                                <div class="radio_item">
                                    <input name="price_type" value="E" type="radio" id="radio1" checked>
                                    <label for="radio1">{{ __('messages.econom') }}</label>
                                </div>
                                <div class="radio_item">
                                    <input name="price_type" value="W" type="radio" id="radio2">
                                    <label for="radio2">{{ __('messages.business_class') }}</label>
                                </div>
                                <div class="radio_item">
                                    <input name="price_type" value="B" type="radio" id="radio3">
                                    <label for="radio3">{{ __('messages.first') }}</label>
                                </div>
                                <div class="radio_item">
                                    <input name="price_type" value="F" type="radio" id="radio4">
                                    <label for="radio4">{{ __('messages.search_all') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="submit_btn">
                        <div class="plane_btn">
                            {{ __('messages.airplane_btn') }}
                            <img src="{{ asset('img/plane.svg') }}">
                        </div>
                        <div class="railway_btn">
                            {{ __('messages.railway_btn') }}
                            <img src="{{ asset('img/tram2.svg') }}">
                        </div>
                    </button>
                </div>
            </div>
        </form>
    </section>
@endsection
