<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="img/favicon.jpg">
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="css/libs.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/media.css">
    <link rel="stylesheet" type="text/css" href="css/cabinet.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/modernizr.js"></script>
    <title>Lokoplane</title>

</head>
<body>
<div class="modal_overlay"></div>

<header class="inner_header">
    <div class="row">
        <a href="/" class="logo_block">
            <h3 class="logo_text"><span>loko</span>plane</h3>
        </a>
        <div class="lang_dropdown">
{{--            <a href="#" class="lang_title">--}}
{{--                русский--}}
{{--                <img src="img/arrow.svg">--}}
{{--            </a>--}}
{{--            <div class="lang_dropdown_block">--}}
{{--                <a href="#">английский</a>--}}
{{--                <a href="#">казахский</a>--}}
{{--            </div>--}}
        </div>
        <div class="account_type">корпоративный клиент</div>
        <a class="log_out" href="{{ url('/logout') }}">{{ __('messages.log_out') }}</a>
{{--        <a class="log_out" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('messages.log_out') }}</a>--}}
{{--        <div class="menu_block">--}}
{{--            <img src="img/menu_violet.svg">--}}
{{--        </div>--}}
    </div>
</header>
@yield('content')
<footer>
    <div class="first_footer">
        <div class="container">
            <div class="main_footer_menu row">
                <div class="menu_block" style="text-align: right; width: 50%; margin-right: 5px;">
                    <h4 style="display: block;"><img src="{{ asset('img/expand.svg') }}">{{ __('messages.for_users') }}</h4>
                    <div class="menu_list">
                        <a href="{{route('post', 'oferta')}}">Публичная оферта</a>
                        <a href="{{route('post', 'privacy')}}">Политика конфиденциальности</a>
                        <a href="{{route('post', 'rules')}}">Правила перелета</a>
                    </div>
                </div>
                <div class="menu_block" style="text-align: left; width: 50%; margin-left: 5px;">
                    <h4>Партнерам<img src="{{ asset('img/expand.svg') }}"></h4>
                    <div class="menu_list">
                        <a href="{{route('corplanding')}}">Корпоративным клиентам</a>
                        <a href="{{route('taplink')}}">Рекламодателям</a>
                        <a href="{{route('taplink')}}">Сотрудничество</a>
                        {{--                            <a href="{{route('corplanding')}}">Стать агентом</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row second_row">
            <div class="footer_apps">
                <a href="#">
                    <img src="img/google.png">
                </a>
                <a href="#">
                    <img src="img/appstore.png">
                </a>
            </div>
        </div>
    </div>

    <!-- HTML-код модального окна-->
    <div id="myModal" class="modal fade">

    </div>
    <div id="overlay"></div><!-- Пoдлoжкa, oднa нa всю стрaницу -->
</footer>
<!-- HTML-код модального окна -->
{{--@include()--}}
<div id="modal_error" class="modal_div">
    <span class="modal_close">X</span>
    <div id="error_text"></div>
</div>

<script src="js/libs.min.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
    $('.form_date').datepicker({
        'format': 'yyyy-mm-dd',
        'orientation': 'top',
        'autoclose': true
    });
    $('#beginning').datepicker({
        'format': 'yyyy-mm-dd',
        'orientation': 'top',
        'autoclose': true
    });
    $('#end').datepicker({
        'format': 'yyyy-mm-dd',
        'orientation': 'top',
        'autoclose': true
    });
    $('#avatar_upload').attr('title', '');
    $('#avatar').click(function () {
        $('#avatar_upload').click();

    });
    $('#avatar_upload').change(function() {
        if(this.files[0].size < 900000){$( "#form_avatar_upload" ).submit();}else{$('#error_text').html('Большой размер файла');$('.open_modal').click();}
    });

    $('#doc_edit_1').click(function() {
        $( "#doc_edit_1" ).hide();
        $( "#doc_1" ).hide();
        $( "#doc_save_1" ).show();
        $( "#doc_form_1" ).show();
    });
    $('#doc_edit_2').click(function() {
        $( "#doc_edit_2" ).hide();
        $( "#doc_2" ).hide();
        $( "#doc_save_2" ).show();
        $( "#doc_form_2" ).show();
    });

</script>
</body>
</html>
