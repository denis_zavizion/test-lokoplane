<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link rel="stylesheet" type="text/css" href="{{ asset('https://fonts.googleapis.com/css?family=Nunito') }}">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.js') }}"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/auth.css') }}">
    <title>Lokoplane</title>
</head>
<body>
<div class="modal_overlay"></div>

<header class="inner_header">
    <div class="row">
        <a href="/" class="logo_block">
            <h3 class="logo_text"><span>loko</span>plane</h3>
        </a>
        <div class="lang_dropdown">
{{--            <a href="#" class="lang_title">--}}
{{--                русский--}}
{{--                <img src="{{ asset('img/arrow.svg') }}">--}}
{{--            </a>--}}
            <div class="lang_dropdown_block">
                <a href="#">английский</a>
                <a href="#">казахский</a>
            </div>
        </div>
        <div class="account_type"><a href="/corp-landing">корпоративный клиент</a></div>
        @auth
            <div class="account_type"><a href="/cabinet">Кабинет</a></div>
            <a class="log_out" href="{{ url('/logout') }}">{{ __('messages.log_out') }}</a>
        @endauth
        @guest
            <a class="log_out" href="{{ url('/cabinet') }}">Вход</a>
        @endguest


{{--        <div class="menu_block">--}}
{{--            <img src="{{ asset('img/menu_violet.svg') }}">--}}
{{--        </div>--}}
    </div>
</header>
@yield('content')
{{--main_footer--}}
<footer>
    <div class="first_footer">
        <div class="container">
            <div class="main_footer_menu row">
                <div class="menu_block" style="text-align: right; width: 50%; margin-right: 5px;">
                    <h4 style="display: block;"><img src="{{ asset('img/expand.svg') }}">{{ __('messages.for_users') }}</h4>
                    <div class="menu_list">
                        <a href="{{route('post', 'oferta')}}">Публичная оферта</a>
                        <a href="{{route('post', 'privacy')}}">Политика конфиденциальности</a>
                        <a href="{{route('post', 'rules')}}">Правила перелета</a>
                    </div>
                </div>
                <div class="menu_block" style="text-align: left; width: 50%; margin-left: 5px;">
                    <h4>Партнерам<img src="{{ asset('img/expand.svg') }}"></h4>
                    <div class="menu_list">
                        <a href="{{route('corplanding')}}">Корпоративным клиентам</a>
                        <a href="{{route('taplink')}}">Рекламодателям</a>
                        <a href="{{route('taplink')}}">Сотрудничество</a>
                        {{--                            <a href="{{route('corplanding')}}">Стать агентом</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row second_row">
            <div class="footer_apps">
                <a href="#">
                    <img src="{{ asset('img/google.png') }}">
                </a>
                <a href="#">
                    <img src="{{ asset('img/appstore.png') }}">
                </a>
            </div>
        </div>
    </div>
</footer>

<script src="{{ asset('js/libs.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('js/mask.js') }}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script src="{{ asset('js/jquery-punch.js') }}"></script>
<script type="text/javascript">
    $('.form_date').datepicker({
        'format': 'yyyy-mm-dd',
        'orientation': 'top',
        'autoclose': true
    });
    $('#beginning').datepicker({
        'format': 'yyyy-mm-dd',
        'orientation': 'top',
        'autoclose': true
    });
    $('#end').datepicker({
        'format': 'yyyy-mm-dd',
        'orientation': 'top',
        'autoclose': true
    });

    function validateEmail(email) {
        var pattern  = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern .test(email);
    }

    function nexPassengers(id){
        var new_id = +id + +1;
        var flag = true;
        if($('#number').val()){$('#number_error').html('');}else{ flag = false; $('#number_error').html('Некорректно заполненое поле') }
        // if($('#extra').val()){$('#extra_error').html('');}else{ flag = false; $('#extra_error').html('Некорректно заполненое поле') }
        if( validateEmail($('#email').val())){$('#email_error').html('');}else{ flag = false; $('#email_error').html('Некорректно заполненое поле') }
        if(id == 0){if(flag){ $('#passager_'+id).hide(); $('#passager_'+new_id).show(); } }

        if(id > 0){
            if( $('#last_name'+id).val()){$('#last_name_error'+id).html('');}else{ flag = false; $('#last_name_error'+id).html('Некорректно заполненое поле') }
            if( $('#first_name'+id).val()){$('#first_name_error'+id).html('');}else{ flag = false; $('#first_name_error'+id).html('Некорректно заполненое поле') }
            if( $('#middle_name'+id).val()){$('#middle_name_error'+id).html('');}else{ flag = false; $('#middle_name_error'+id).html('Некорректно заполненое поле') }
            if( $('#birth_date'+id).val()){$('#birth_date_error'+id).html('');}else{ flag = false; $('#birth_date_error'+id).html('Некорректно заполненое поле') }
            if( $('#number_document'+id).val()){$('#number_document_error'+id).html('');}else{ flag = false; $('#number_document_error'+id).html('Некорректно заполненое поле') }
            if( $('#citizenship'+id).val()){$('#citizenship_error'+id).html('');}else{ flag = false; $('#citizenship_error'+id).html('Некорректно заполненое поле') }
            if( $('#Amadeus'+id).val()){$('#Amadeus_error'+id).html('');}else{ flag = false; $('#Amadeus_error'+id).html('Некорректно заполненое поле') }
            if( $('#expire'+id).val()){$('#expire_error'+id).html('');}else{ flag = false; $('#expire_error'+id).html('Некорректно заполненое поле') }
            if(flag){ $('#passager_'+id).hide(); $('#passager_'+new_id).show();}
        }

        if (id != 0){
            let pass = $('#pass_'+id).data( "pass" );
            $('#type'+id).val(pass);

            passengers = $('#passengers').val();
            $('#pass_'+id).html('Заполнено');

            if(flag && (id == passengers)){
                $("#loading").show();
                $("#book_now").hide();
                document.getElementById("offer_passenger").submit();
            }
            // $.each($('.pass_res'),function(index,data) {
            //     console.log('Порядковый номер: ' + index + ' ; Содержимое: ' +$(data).text());
            // });
        }

        if($('#number').val()){$('#number_error').html('');}else{ flag = false; $('#number_error').html('Некорректно заполненое поле') }
    };


    // $('.phone').mask('(777) 000-00-00', {clearMaskOnLostFocus: false});
    $(".route_range").slider({
        range: true,
        min: 0,
        max: 2880,
        step: 5,
        values: [0, 2880],
        slide: function (e, ui) {
            var hours1 = Math.floor(ui.values[0] / 60);
            var minutes1 = ui.values[0] - (hours1 * 60);

            if (hours1 < 10) hours1 = '0' + hours1;
            if (minutes1 < 10) minutes1 = '0' + minutes1;
            if (minutes1 == 0) minutes1 = '00';

            if (hours1 == 24){
                hours1 = '00';
                minutes1 = '00';
            }

            $(this).parents('.filter_block').find('.min_hours').text(hours1);
            $(this).parents('.filter_block').find('.min_minutes').text(minutes1);
            $(this).parents('.filter_block').find('.min_input').val(hours1 + ':' + minutes1);

            var hours2 = Math.floor(ui.values[1] / 60);
            var minutes2 = ui.values[1] - (hours2 * 60);

            if (hours2 < 10) hours2 = '0' + hours2;
            if (minutes2 < 10) minutes2 = '0' + minutes2;
            if (minutes2 == 0) minutes2 = '00';
            if (hours2 == 24){
                hours2 = '00';
                minutes2 = '00';
            }

            $(this).parents('.filter_block').find('.max_hours').text(hours2);
            $(this).parents('.filter_block').find('.max_minutes').text(minutes2);
            $(this).parents('.filter_block').find('.max_input').val(hours2 + ':' + minutes2);
        }
    });
    $(".time_range").slider({
        range: true,
        min: 0,
        max: 1440,
        step: 5,
        values: [0, 1440],
        slide: function (e, ui) {
            var hours1 = Math.floor(ui.values[0] / 60);
            var minutes1 = ui.values[0] - (hours1 * 60);

            if (hours1 < 10) hours1 = '0' + hours1;
            if (minutes1 < 10) minutes1 = '0' + minutes1;
            if (minutes1 == 0) minutes1 = '00';

            if (hours1 == 24){
                hours1 = '00';
                minutes1 = '00';
            }

            $(this).parents('.item').find('.min').text(hours1 + ':' + minutes1);
            $(this).parents('.item').find('.min_input').val(hours1 + ':' + minutes1);

            var hours2 = Math.floor(ui.values[1] / 60);
            var minutes2 = ui.values[1] - (hours2 * 60);

            if (hours2 < 10) hours2 = '0' + hours2;
            if (minutes2 < 10) minutes2 = '0' + minutes2;
            if (minutes2 == 0) minutes2 = '00';
            if (hours2 == 24){
                hours2 = '00';
                minutes2 = '00';
            }

            $(this).parents('.item').find('.max').text(hours2 + ':' + minutes2);
            $(this).parents('.item').find('.max_input').val(hours2 + ':' + minutes2);
        }
    });
    $( ".price_range" ).slider({
        range: true,
        orientation: "horizontal",
        min: 0,
        max: 50000,
        values: [0, 500000],
        step: 50,
        slide: function (event, ui) {
            if (ui.values[0] == ui.values[1]) {
                return false;
            }
            $(this).parents('.filter_block').find(".min_input").val(ui.values[0]);
            $(this).parents('.filter_block').find(".max_input").val(ui.values[1]);

            $(this).parents('.filter_block').find(".min").text(ui.values[0]);
            $(this).parents('.filter_block').find(".max").text(ui.values[1]);
        }
    });
</script>
</body>
</html>


