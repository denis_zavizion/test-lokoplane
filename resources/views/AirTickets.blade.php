@extends('layouts.app')

@section('content')
    <section class="inner_search header_search">
        <form action="/air-tickets/" method="get">
            <div class="row">
                <div class="input_row">
{{--                    <button type="button" class="additional_btn plane_btn" style="display: block">{{ __('messages.multiple_flights') }}</button>--}}
                    <button type="button" class="additional_btn tram_btn">{{ __('messages.timetable') }}</button>
                    <div class="toggler">
                        <input type="radio" name="search_type" id="avia" value="avia" checked>
                        <label for="avia" class="first">
                            <img src="{{ asset('img/plane.svg') }}">
                        </label>
{{--                        <input type="radio" name="search_type" id="railway" value="railway">--}}
{{--                        <label for="railway">--}}
{{--                            <img src="{{ asset('img/tram.svg') }}">--}}
{{--                        </label>--}}
                    </div>
                    <div class="banner_input dropdown_input where_from">
                        <img src="{{ asset('img/exchange.svg') }}" class="exchange hide_md">
                        <img src="{{ asset('img/exchange_mobile.svg') }}" class="exchange show_md">
                        <p>{{ __('messages.from') }}</p>
                        <div class="dropdown_title">
                            <input type="text" class="search_input city_input" value="{{$cities[$code['from_input']]}}">
                            <span>{{$code['from_input']}}</span>
                        </div>
                        <div class="dropdown_block">
                            <div class="dropdown_list">
                                @foreach($cities as $key=>$city)
                                    <div class="dropdown_item" data-input-value="{{$city}}" data-search-keyword="{{$city}}, {{$key}}">
                                        <h4>{{$city}}</h4>
                                        <p>{{$key}}</p>
                                    </div>
                                @endforeach
                            </div>
                            <input type="hidden" class="hidden_input">
                            <input name="from_input" type="hidden" class="hidden_input from_to_input" value="{{$code['from_input']}}">
                        </div>
                    </div>
                    <div class="banner_input dropdown_input where_from where_input">
                        <p>{{ __('messages.where') }}</p>
                        <div class="dropdown_title">
                            <input type="text" class="search_input city_input" value="{{$cities[$code['to_input']]}}">
                            <span>{{$code['to_input']}}</span>
                        </div>
                        <div class="dropdown_block">
                            <div class="dropdown_list">
                                @foreach($cities as $key=>$city)
                                    <div class="dropdown_item" data-input-value="{{$city}}" data-search-keyword="{{$city}}, {{$key}}">
                                        <h4>{{$city}}</h4>
                                        <p>{{$key}}</p>
                                    </div>
                                @endforeach
                            </div>
                            <input type="hidden" class="hidden_input">
                            <input name="to_input" type="hidden" class="hidden_input from_to_input" value="{{$code['to_input']}}">
                        </div>
                    </div>
                    <div class="banner_input start date">
                        <p>дата</p>
                        <input name="beginning" type="text" id="beginning" value="{{$beginning}}">
                        <img src="{{ asset('img/calendar.svg') }}" class="icon">
                    </div>
                    <div class="banner_input date finish_block">
                        <p>{{ __('messages.end_route') }}</p>
                        <img src="{{ asset('img/calendar.svg') }}" class="icon">
                        <input name="end" type="text" id="end" value="{{$end}}">
                    </div>
                    <div class="banner_input dropdown_input types_dropdown">
                        <p>{{ __('messages.amount') }}</p>
                        <div class="dropdown_title">
                            <span data-text="adults">{{$code['adults']}}</span> {{ __('messages.adults_short') }},
                            <span data-text="child">{{$code['child']}}</span> {{ __('messages.children_short') }},
                            <span data-text="baby">{{$code['baby']}}</span> {{ __('messages.babies_short') }},
                            <span class="type trip_type last">{{ __('messages.econom') }}</span>
                        </div>
                        <div class="dropdown_block">
                            <div class="dropdown_content">
                                <div class="item">
                                    <div class="item_text">
                                        <h4>{{ __('messages.adults') }}</h4>
                                        <p>{{ __('messages.older_12') }}</p>
                                    </div>
                                    <div class="amount">
                                        <button type="button" class="minus" data-text-output="adults">-</button>
                                        <input name="adults" type="text" value="{{$code['adults']}}" readonly>
                                        <button type="button" class="plus" data-text-output="adults">+</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item_text">
                                        <h4>Дети</h4>
                                        <p>{{ __('messages.younger_12') }}</p>
                                    </div>
                                    <div class="amount">
                                        <button type="button" class="minus" data-text-output="child">-</button>
                                        <input name="child" type="text" value="{{$code['child']}}" readonly>
                                        <button type="button" class="plus" data-text-output="child">+</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item_text">
                                        <h4>Младенцы</h4>
                                        <p>{{ __('messages.younger_2') }}</p>
                                    </div>
                                    <div class="amount">
                                        <button type="button" class="minus" data-text-output="baby">-</button>
                                        <input name="baby" type="text" value="{{$code['baby']}}" readonly>
                                        <button type="button" class="plus" data-text-output="baby">+</button>
                                    </div>
                                </div>
                            </div>
                            <div class="radio_btns">
                                <div class="radio_item">
                                    <input name="price_type" value="E" type="radio" id="radio1" checked>
                                    <label for="radio1">{{ __('messages.econom') }}</label>
                                </div>
                                <div class="radio_item">
                                    <input name="price_type" value="W" type="radio" id="radio2">
                                    <label for="radio2">{{ __('messages.business_class') }}</label>
                                </div>
                                <div class="radio_item">
                                    <input name="price_type" value="B" type="radio" id="radio3">
                                    <label for="radio3">{{ __('messages.first') }}</label>
                                </div>
                                <div class="radio_item">
                                    <input name="price_type" value="F" type="radio" id="radio4">
                                    <label for="radio4">{{ __('messages.search_all') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <search-btn-component></search-btn-component>--}}
                    <button type="submit" class="submit_btn">
                        <div class="plane_btn">
                            {{ __('messages.airplane_btn') }}
                            <img src="{{ asset('img/plane.svg') }}">
                        </div>
                        <div class="railway_btn">
                            {{ __('messages.railway_btn') }}
                            <img src="{{ asset('img/tram2.svg') }}">
                        </div>
                    </button>
                </div>
            </div>
        </form>
    </section>

    <div id="app"><tickets-component></tickets-component></div>
{{--    <script src="{{asset('js/app.js')}}"></script>--}}
@endsection

