@extends('layouts.app')

@section('content')
    <section class="inner_section cabinet_section">
        <div class="row">
            <div class="left_block result_body width_100_md" id="app">
                <div class="air_item mobile_air_item">
                    <div class="air_price">
                        <h4>{{$tickets->price}} тг</h4>
                        <p>цена за @if(isset($tickets->price_details[0]->qty)){{$tickets->price_details[0]->qty}} @endif пассажира</p>
                    </div>
                    <div class="air_desc">
                        <div class="desc_row">
                            <div class="start air_date">
                                <h4>{{$tickets->departure_time}}</h4>
                                <h5>{{$directions[0]->departure_name}}</h5>
                            </div>
                            <div class="route_info">
                                <p class="first_route_date">{{$tickets->departure_date}}</p>
                                <div class="route_row">
                                    @foreach($tickets->flights_info as $segm)
                                        <h4>{{$segm->departure_airport}}</h4>
                                    @endforeach
                                    @if($tickets->arrival_airport)
                                        <h4>{{$segm->arrival_airport}}</h4>
                                    @endif
                                </div>
                                <p class="second_route_date">{{$tickets->arrival_date}}</p>
                            </div>
                            <div class="end_air_date air_date">
                                <h4>{{$tickets->arrival_time}}</h4>
                                <h5>{{$directions[0]->arrival_name}}</h5>
                            </div>
                        </div>

                        <div class="more_route_info" style="display: block;">
                            <div class="flex_row">
                                <div class="route_desc_column">
                                    @foreach($tickets->flights_info as $k_y=>$segm)
                                        @if($k_y > 0)
                                        <p class="note">пересадка {{$tickets->flights_info[$k_y-1]->stop_time}} в {{$tickets->flights_info[$k_y-1]->departure_city}}</p>
                                        @endif
                                        <h4 class="route_item_mobile">{{$tickets->flights_info[$k_y]->departure_local_time}}</h4>
                                        <div class="route_item">
                                            <div class="route_left">
                                                <h4>{{$segm->departure_local_time}} {{$segm->departure_airport}} {{$segm->departure_city}}</h4>
                                                <h4>{{$segm->arrival_local_time}} {{$segm->arrival_airport}} {{$segm->arrival_city}}</h4>
                                            </div>
                                            <div class="route_right">
                                                <img src="{{$segm->operating_airline_logo}}" class="logo">
                                                <p>{{$segm->departure_city}}<br>
                                                    {{$segm->airplane_code}}<br>
                                                    {{$segm->airplane_name}}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($status)
                <div id="loading" style="display: none"><img src="/img/preloader-upload.svg"></div>
                <div class="main_block width_100_md" id="book_now">
                    <div class="order_form_block">
                        <h3 class="small_title">Забронировать билет </h3>
                        <div class="passengers_block">
                            <div class="passengers_block_2">
                                @php $pass_id = 0; @endphp
                                @for ($adults_id =1; $adults_id <= $adults; $adults_id++)
                                    @php $pass_id++; @endphp
                                    <div>{{$adults_id}}-й Взрослый - <div class="pass_res" data-pass="ADT" id="pass_{{$pass_id}}">Не заполнено</div> </div>
                                @endfor
                                @for ($child_id =1; $child_id <= $child; $child_id++)
                                    @php $pass_id++; @endphp
                                    <div>{{$child_id}}-й Ребенок - <div class="pass_res"  data-pass="CHD" id="pass_{{$pass_id}}">Не заполнено</div> </div>
                                @endfor
                                @for ($baby_id =1; $baby_id <= $baby; $baby_id++)
                                    @php $pass_id++; @endphp
                                    <div>{{$baby_id}}-й Младенец - <div class="pass_res"  data-pass="INF" id="pass_{{$pass_id}}">Не заполнено</div> </div>
                                @endfor
                            </div>
                        </div>
                        <form id="offer_passenger" class="order_input_row" method="GET" action="{{ route('buy_info') }}" autocomplete="off">
                            <input type="hidden" name="buy_id" value="{{$buy_id}}">
                            <input type="hidden" name="request_id" value="{{$request_id}}">
                            <input type="hidden" id="code" name="code" value="{{$departure_country}}">
                            <input type="hidden" id="passengers" name="passengers" value="{{$passengers}}">

                            <input type="hidden" id="passengers" name="passengers" value="{{$passengers}}">
                            @for ($i =0; $i <= $passengers; $i++)
                                @if($i == 0)
                                    <div id="passager_{{$i}}" class="passager">
                                        <div class="order_input phone_input first_phone">
                                            <p>Номер телефона</p>
                                            <div class="dropdown_input order_input more_z3">
                                                <div class="dropdown_title">
                                                    <input type="text" class="search_input" value="+7">
                                                </div>
                                                <div class="dropdown_block">
                                                    <div class="dropdown_list">
                                                        @include('add.number')
                                                    </div>
                                                </div>
                                            </div>
                                            <input name="number" id="number" type="text" class="phone" placeholder="000-000-000" value="{{ isset($session['number']) ? $session['number'] : ''}}">
                                            <div class="error_offers" id="number_error">{{$errors->first('number','некорректно введенны данные')}}</div>
                                        </div>
                                        <div class="order_input phone_input second_phone">
                                            <p>Дополнительный номер телефона</p>
                                            <div class="dropdown_input order_input more_z4">
                                                <div class="dropdown_title">
                                                    <input type="text" class="search_input" value="+7">
                                                </div>
                                                <div class="dropdown_block">
                                                    <div class="dropdown_list">
                                                        @include('add.number')
                                                    </div>
                                                </div>
                                            </div>
                                            <input name="extra" id="extra" type="number" class="phone"  value="{{ isset($session['extra']) ? $session['extra'] : ''}}">
                                        </div>
                                        <div class="order_input">
                                            <p>Почта</p>
                                            <input name="email" id="email" type="text" placeholder="vasha-pochta@email.com" value="{{ isset($session['email']) ? $session['email'] : ''}}">
                                            <div class="error_offers" id="email_error">{{$errors->first('email','некорректно введенны данные')}}</div>
                                        </div>
                                        <button type="button" class="submit_btn" onclick="nexPassengers({{$i}})">Перейти дальше<img src="/img/arrow_violet.svg"></button>
                                    </div>
                                @elseif($i != 0)
                                    <input name="type[{{$i}}]" id="type{{$i}}" type="hidden" value="ADT">
                                    <div id="passager_{{$i}}" class="passager" @if($i != 0) style="display: none" @endif>
                                        <div class="input_half_block">
                                            <div class="order_input">
                                                <p>Ваша фамилия</p>
                                                <input name="last_name[{{$i}}]" id="last_name{{$i}}" type="text" placeholder="Ivanov" value="{{ isset($session['last_name'][$i]) ? $session['last_name'][$i] : ''}}">
                                                <span>Все данные заполняются латиницей</span>
                                                <div class="error_offers" id="last_name_error{{$i}}">{{$errors->first('last_name','некорректно введенны данные')}}</div>
                                            </div>
                                            <div class="order_input">
                                                <p>Ваше имя</p>
                                                <input name="first_name[{{$i}}]" id="first_name{{$i}}" type="text" placeholder="Ivan" value="{{ isset($session['first_name'][$i]) ? $session['first_name'][$i] : ''}}">
                                                <span>Все данные заполняются латиницей</span>
                                                <div class="error_offers"  id="first_name_error{{$i}}">{{$errors->first('first_name','некорректно введенны данные')}}</div>
                                            </div>
                                            <div class="order_input name_input">
                                                <p>Отчество</p>
                                                <input name="middle_name[{{$i}}]" id="middle_name{{$i}}" type="text" placeholder="Ivanovich" value="{{ isset($session['middle_name'][$i]) ? $session['middle_name'][$i] : ''}}">
                                                <span>Уважаемые пассажиры, если в вашем полетном сегменте присутствует российский
                                            аэропорт, то необходимо вводить отчество латинскими буквами.</span>
                                                <div class="error_offers" id="middle_name_error{{$i}}">{{$errors->first('middle_name','некорректно введенны данные')}}</div>
                                            </div>
                                            <div class="order_input">
                                                <p>Дата рождения</p>
                                                <input name="birth_date[{{$i}}]" id="birth_date{{$i}}" type="text" class="form_date"  placeholder="дд.мм.гггг" value="{{ isset($session['birth_date'][$i]) ? $session['birth_date'][$i] : ''}}">
                                                <img src="/img/calendar.svg" class="icon">
                                                <div class="error_offers" id="birth_date_error{{$i}}">{{$errors->first('birth_date','некорректно введенны данные')}}</div>
                                            </div>
                                        </div>
                                        <div class="input_half_block">
                                            <div class="dropdown_input order_input more_z4">
                                                <p>Гражданство</p>
                                                <div class="dropdown_title">
                                                    <input type="text" class="search_input city_input">
                                                </div>
                                                <div class="dropdown_block">
                                                    <div class="dropdown_list">
                                                        @include('city')
                                                    </div>
                                                    <input name="citizenship[{{$i}}]" id="citizenship{{$i}}" type="hidden" class="hidden_input">
                                                </div>
                                                <div class="error_offers" id="citizenship_error{{$i}}">{{$errors->first('citizenship','некорректно введенны данные')}}</div>
                                            </div>
                                            <div class="dropdown_input order_input more_z5">
                                                <p>Документ</p>
                                                <div class="dropdown_title">
                                                    <input type="text" class="search_input city_input">
                                                </div>
                                                <div class="dropdown_block">
                                                    <div class="dropdown_list">
                                                        <div class="dropdown_item" data-input-value="NI" data-search-keyword=""><h4>Удостоверение личности KAZ</h4></div>
                                                        <div class="dropdown_item" data-input-value="PSP" data-search-keyword=""><h4>Заграничный паспорт</h4></div>
                                                        <div class="dropdown_item" data-input-value="PS" data-search-keyword=""><h4>Паспорт внутренний</h4></div>
                                                        <div class="dropdown_item" data-input-value="NP" data-search-keyword=""><h4>Национальный паспорт</h4></div>
                                                        <div class="dropdown_item" data-input-value="SR" data-search-keyword=""><h4>Свидетельство о рождении</h4></div>
                                                        <div class="dropdown_item" data-input-value="VB" data-search-keyword=""><h4>Военный билет</h4></div>
                                                        <div class="dropdown_item" data-input-value="DP" data-search-keyword=""><h4>Дипломатический паспорт</h4></div>
                                                        <div class="dropdown_item" data-input-value="SP" data-search-keyword=""><h4>Служебный паспорт</h4></div>
                                                        <div class="dropdown_item" data-input-value="PM" data-search-keyword=""><h4>Паспорт моряка</h4></div>
                                                        <div class="dropdown_item" data-input-value="UD" data-search-keyword=""><h4>Удостоверение депутата совета Федерации</h4></div>
                                                        <div class="dropdown_item" data-input-value="UDL" data-search-keyword=""><h4>Удостоверение личности военнослужащего РФ</h4></div>
                                                        <div class="dropdown_item" data-input-value="SPO" data-search-keyword=""><h4>Справка об освобождении из мест лишения свободы</h4></div>
                                                        <div class="dropdown_item" data-input-value="VUL" data-search-keyword=""><h4>Удостоверение осужденного на выезд за пределы мест...</h4></div>
                                                        <div class="dropdown_item" data-input-value="SPU" data-search-keyword=""><h4>Временное удостоверение личности</h4></div>
                                                        <div class="dropdown_item" data-input-value="VV" data-search-keyword=""><h4>Вид на жительство</h4></div>
                                                        <div class="dropdown_item" data-input-value="CVV" data-search-keyword=""><h4>Свидетельство на возвращение в страны СНГ</h4></div>
                                                    </div>
                                                    <input name="Amadeus[{{$i}}]" id="Amadeus{{$i}}" type="hidden" class="hidden_input">
                                                </div>
                                                <div class="error_offers" id="Amadeus_error{{$i}}">{{$errors->first('Amadeus','некорректно введенны данные')}}</div>
                                            </div>
                                            <div class="order_input">
                                                <p>Номер документа</p>
                                                <input name="number_document[{{$i}}]" id="number_document{{$i}}" type="text" placeholder="" value="{{ isset($session['number_document'][$i]) ? $session['number_document'][$i] : ''}}">
                                                <span>Все данные заполняются латиницей</span>
                                                <div class="error_offers" id="number_document_error{{$i}}">{{$errors->first('number_document','некорректно введенны данные')}}</div>
                                            </div>
                                            <div class="order_input">
                                                <p>Дата окончания срока действия паспорта</p>
                                                <input name="expire[{{$i}}]" id="expire{{$i}}" type="text" class="form_date" placeholder="дд.мм.гггг" value="{{ isset($session['expire'][$i]) ? $session['expire'][$i] : ''}}">
                                                <img src="/img/calendar.svg" class="icon">
                                                <div class="error_offers" id="expire_error{{$i}}"></div>
                                            </div>
                                            <div class="dropdown_input order_input more_z6">
                                                <p>Пол</p>
                                                <div class="dropdown_title">
                                                    <input type="text" class="search_input">
                                                </div>
                                                <div class="dropdown_block">
                                                    <div class="dropdown_list">
                                                        <div class="dropdown_item" data-input-value="M" data-search-keyword=""><h4>Мужчина</h4></div>
                                                        <div class="dropdown_item" data-input-value="F" data-search-keyword=""><h4>Женщина</h4></div>
                                                    </div>
                                                    <input name="gender[{{$i}}]" id="gender{{$i}}" type="hidden" class="hidden_input">
                                                </div>
                                                <div class="error_gender" id="gender_error{{$i}}">{{$errors->first('gender','некорректно введенны данные')}}</div>
                                            </div>
{{--                                            @if($i == $passengers)--}}
{{--                                                <button id="submit_pay" type="submit" class="submit_btn">Перейти к оплате<img src="/img/arrow_violet.svg"></button>--}}
{{--                                            @else--}}
                                                <button type="button" class="submit_btn" onclick="nexPassengers({{$i}})">Перейти дальше<img src="/img/arrow_violet.svg"></button>
{{--                                            @endif--}}
                                        </div>
                                    </div>
                                @endif
                            @endfor
                        </form>
                    </div>
                </div>
            @else
                Что то пошло не так!
                Повторите запрос.
            @endif
        </div>
    </section>
    <section class="inner_search header_search mb-0">
        <form action="/air-tickets/" method="get">
            <div class="row">
                <div class="input_row">
                    {{--                    <button type="button" class="additional_btn plane_btn" style="display: block">{{ __('messages.multiple_flights') }}</button>--}}
                    <button type="button" class="additional_btn tram_btn">{{ __('messages.timetable') }}</button>
                    <div class="toggler">
                        <input type="radio" name="search_type" id="avia" value="avia" checked>
                        <label for="avia" class="first">
                            <img src="{{ asset('img/plane.svg') }}">
                        </label>
                        {{--                        <input type="radio" name="search_type" id="railway" value="railway">--}}
                        {{--                        <label for="railway">--}}
                        {{--                            <img src="{{ asset('img/tram.svg') }}">--}}
                        {{--                        </label>--}}
                    </div>
                    <div class="banner_input dropdown_input where_from">
                        <img src="{{ asset('img/exchange.svg') }}" class="exchange hide_md">
                        <img src="{{ asset('img/exchange_mobile.svg') }}" class="exchange show_md">
                        <p>{{ __('messages.from') }}</p>
                        <div class="dropdown_title">
                            <input type="text" class="search_input city_input" value="Нур-Султан, Казахстан">
                            <span>{{ env('city_from_code') }}</span>
                        </div>
                        <div class="dropdown_block">
                            <div class="dropdown_list">
                                @foreach($cities as $key=>$city)
                                    <div class="dropdown_item" data-input-value="{{$city}}"
                                         data-search-keyword="{{$city}}, {{$key}}">
                                        <h4>{{$city}}</h4>
                                        <p>{{$key}}</p>
                                    </div>
                                @endforeach
                            </div>
                            <input type="hidden" class="hidden_input">
                            <input name="from_input" type="hidden" class="hidden_input from_to_input" value="{{ env('city_from_code') }}">
                        </div>
                    </div>
                    <div class="banner_input dropdown_input where_from where_input">
                        <p>{{ __('messages.where') }}</p>
                        <div class="dropdown_title">
                            <input type="text" class="search_input city_input" value="Алматы, Казахстан">
                            <span>{{ env('city_to_code') }}</span>
                        </div>
                        <div class="dropdown_block">
                            <div class="dropdown_list">
                                @foreach($cities as $key=>$city)
                                    <div class="dropdown_item" data-input-value="{{$city}}"
                                         data-search-keyword="{{$city}}, {{$key}}">
                                        <h4>{{$city}}</h4>
                                        <p>{{$key}}</p>
                                    </div>
                                @endforeach
                            </div>
                            <input type="hidden" class="hidden_input">
                            <input name="to_input" type="hidden" class="hidden_input from_to_input" value="{{ env('city_to_code') }}">
                        </div>
                    </div>
                    <div class="banner_input start date">
                        <p>дата</p>
                        <input name="beginning" type="text" id="beginning" value="{{$beginning}}">
                        <img src="{{ asset('img/calendar.svg') }}" class="icon">
                    </div>
                    {{--                <div class="banner_input date finish_block">--}}
                    {{--                    <p>{{ __('messages.end_route') }}</p>--}}
                    {{--                    <img src="{{ asset('img/calendar.svg') }}" class="icon">--}}
                    {{--                    <input name="end" type="text" id="end" value="{{$end}}">--}}
                    {{--                </div>--}}
                    <div class="banner_input dropdown_input types_dropdown">
                        <p>{{ __('messages.amount') }}</p>
                        <div class="dropdown_title">
                            <span data-text="adults">1 </span> {{ __('messages.adults_short') }},
                            <span data-text="child">0 </span> {{ __('messages.children_short') }},
                            <span data-text="baby">0 </span> {{ __('messages.babies_short') }},
                            <span class="type trip_type last">{{ __('messages.econom') }}</span>
                        </div>
                        <div class="dropdown_block">
                            <div class="dropdown_content">
                                <div class="item">
                                    <div class="item_text">
                                        <h4>{{ __('messages.adults') }}</h4>
                                        <p>{{ __('messages.older_12') }}</p>
                                    </div>
                                    <div class="amount">
                                        <button type="button" class="minus" data-text-output="adults">-</button>
                                        <input name="adults" type="text" value="1" readonly>
                                        <button type="button" class="plus" data-text-output="adults">+</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item_text">
                                        <h4>{{ __('Дети') }}</h4>
                                        <p>{{ __('messages.younger_12') }}</p>
                                    </div>
                                    <div class="amount">
                                        <button type="button" class="minus" data-text-output="child">-</button>
                                        <input name="child" type="text" value="0" readonly>
                                        <button type="button" class="plus" data-text-output="child">+</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item_text">
                                        <h4>{{ __('Младенцы') }}</h4>
                                        <p>{{ __('messages.younger_2') }}</p>
                                    </div>
                                    <div class="amount">
                                        <button type="button" class="minus" data-text-output="baby">-</button>
                                        <input name="baby" type="text" value="0" readonly>
                                        <button type="button" class="plus" data-text-output="baby">+</button>
                                    </div>
                                </div>
                            </div>
                            <div class="radio_btns">
                                <div class="radio_item">
                                    <input name="price_type" value="E" type="radio" id="radio1" checked>
                                    <label for="radio1">{{ __('messages.econom') }}</label>
                                </div>
                                <div class="radio_item">
                                    <input name="price_type" value="W" type="radio" id="radio2">
                                    <label for="radio2">{{ __('messages.business_class') }}</label>
                                </div>
                                <div class="radio_item">
                                    <input name="price_type" value="B" type="radio" id="radio3">
                                    <label for="radio3">{{ __('messages.first') }}</label>
                                </div>
                                <div class="radio_item">
                                    <input name="price_type" value="F" type="radio" id="radio4">
                                    <label for="radio4">{{ __('messages.search_all') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--                    <search-btn-component></search-btn-component>--}}
                    <button type="submit" class="submit_btn">
                        <div class="plane_btn">
                            {{ __('messages.airplane_btn') }}
                            <img src="{{ asset('img/plane.svg') }}">
                        </div>
                        <div class="railway_btn">
                            {{ __('messages.railway_btn') }}
                            <img src="{{ asset('img/tram2.svg') }}">
                        </div>
                    </button>
                </div>
            </div>
        </form>
    </section>

@endsection
