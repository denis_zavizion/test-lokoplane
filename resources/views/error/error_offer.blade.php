@extends('layouts.app')

@section('content')

<section class="inner_section cabinet_section">
    <div class="row">
        <div class="main_block width_100_md" style="width: 100%;">
            <div class="order_form_block">
                <h3 class="small_title">Что-то пошло не так!</h3>
                <div> {{$messages}} <img src="https://lokoplane.kz/img/smile3.png" width=50px/></div>
                <br>
                <a href="https://lokoplane.kz">
                <button type="submit" class="submit_btn">
            <div class="plane_btn">
                Искать
                <img src="https://lokoplane.kz/img/plane.svg">
            </div>
        </button>
    </a>
            </div>
        </div>
    </div>
</section>
@endsection

