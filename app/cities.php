<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cities extends Model
{
    protected $table = "cities";
    protected $primaryKey = 'city_id';
    protected $fillable = ['city_id', 'city', 'code'];
}
