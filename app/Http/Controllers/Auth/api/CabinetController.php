<?php

namespace App\Http\Controllers\Auth\api;

use App\cities;
use App\Http\Controllers\AirTicketsController;
use App\Http\Controllers\Controller;
use App\passports;
use App\payment_history;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Validation\Validator;


class CabinetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index(){

        $id = Auth::user()->id;
        $email = Auth::user()->email;

        $params_page['tickets'] = payment_history::where('status', '=', 'active')
            ->join('tickets', 'tickets.buy_id', '=', 'payment_history.buy_id')
            ->where(function($query) use ($email, $id) {
                $query->where('payment_history.user_id', '=', $id)
                    ->orWhere('payment_history.email', '=', $email);
            })->get();
        $params_page['tickets_off'] = payment_history::where('updated_at', '>=', Carbon::now()->subDays(2)->toDateTimeString())->where('status', '=', 'pennding')
            ->join('tickets', 'tickets.buy_id', '=', 'payment_history.buy_id')
            ->where(function($query) use ($email, $id) {
                $query->where('payment_history.user_id', '=', $id)
                    ->orWhere('payment_history.email', '=', $email);
            })->get();

        $beginning = date("Y-m-d", time()+24*60*60*7);
        $end = '';
        $code['adults'] = 1;
        if(isset($request->beginning)){$beginning=$request->beginning;}
        if(isset($request->end)){$end=$request->end;}
        $air = new AirTicketsController();
        $params_page['users'] = User::find($id);
        if(isset($params_page['users']->type_document) && $params_page['users']->type_document){
            $passports = passports::where('code', $params_page['users']->type_document)->get();
            $params_page['passport_code'] = $passports[0]->code;
            $params_page['passport'] = $passports[0]->passport;
        }else{
            $params_page['passport_code'] = '';
            $params_page['passport'] = '';
        }

        $params_page['beginning'] = $beginning;
        $params_page['end'] = $end;
        $params_page['code'] = $code;
        $params_page['avatar'] = Auth::user()->avatar;
        $params_page['i'] = 0;

        return $params_page;
    }

    public function AvatarUpload(Request $request){
        $user_id = Auth::user()->id;

        $img = '';
        $request->validate(['avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048' ]);
        if(isset($request->avatar) && $request->avatar){
            if($request->hasFile('avatar')) {
                $file = $request->file('avatar');
                $img=time().'.'.$file->getClientOriginalName();
                $user = User::find($user_id);
                $user->avatar = $img;
                $user->save();
                $file->move(public_path() . '/images/avatar/'.$user_id, $img);
            }
        }
        return ['img'=>$img];
    }

    public function doc_cabinet(Request $request){
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        if(isset($request->last_name)){$user->last_name = $request->last_name;}
        if(isset($request->first_name)){$user->first_name = $request->first_name;}
        if(isset($request->middle_name)){$user->middle_name = $request->middle_name;}
        if(isset($request->birth_date)){$user->birth_date = $request->birth_date;}
        if(isset($request->phone)){$user->phone = $request->phone;}
        if(isset($request->email)){$user->email = $request->email;}
        if(isset($request->citizenship)){$user->citizenship = $request->citizenship;}
        if(isset($request->type_document)){$user->type_document = $request->type_document;}
        if(isset($request->number_document)){$user->number_document = $request->number_document;}
        if(isset($request->expire)){$user->expire = $request->expire;}
        if(isset($request->gender)){$user->gender = $request->gender;}
        $user->save();

        return ['status'=>1];
    }


}
