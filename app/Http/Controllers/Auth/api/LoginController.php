<?php

namespace App\Http\Controllers\Auth\api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Validation\Validator;


class LoginController extends Controller
{
    public function login(Request $request){
        $login = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
        if(!Auth::attempt($login)){
            return response(['message' => 'Неправильный логин и пароль', 'status' => 0]);
        }
        $accessToken = Auth::user()->createToken('authToken')->accessToken;
        Auth::user()->update(['api_token'=> $accessToken]);
        return response(['status' => 1, 'user' => Auth::user(), 'access_token' => $accessToken]);
    }

//    public function validator($data)
//    {
//        return Validator::make($data, [
//            'name' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255'],
//            'password' => ['required', 'string', 'min:8', 'confirmed']
//        ]);
//    }
    public function register(Request $request)
    {

        $register = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user_exist = User::where('email', $request->email)->orwhere('name', $request->name)->first();

        if(!isset($register) || $user_exist){
            return ['message' => 'Регистрация не удалась', 'status' => 0];
        }else{
            $user= User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            if(isset($user) && $user){
                return ['status' => 1, 'user'=>$user];
            }else{return ['message' => 'Регистрация не удалась', 'status' => 0];}
        }
    }
}
