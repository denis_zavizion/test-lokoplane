<?php

namespace App\Http\Controllers\Auth\api;

use App\cities;
use App\Http\Controllers\AirTicketsController;
use App\Http\Controllers\Controller;
use App\passports;
use App\payment_history;
use App\tickets;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Validation\Validator;


class BiletController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function payment_history_pennding(Request $request){
        /*Записать в БД order_id*/
        $user_id = Auth::user()->id;
        if(isset($user_id) && $user_id){
            $first_name=json_encode($request->first_name);
            $last_name=json_encode($request->last_name);
            if(isset($request->middle_name) && $request->middle_name){
                $middle_name=json_encode($request->middle_name);
            }else{$middle_name='';}

            $birth_date=json_encode($request->birth_date);
            $citizenship=json_encode($request->citizenship);
            $expire=json_encode($request->expire);
            if(isset($request->extra) && $request->extra){$extra = $request->extra;}else{$extra = '';}


            if(
                isset($request->buy_id) && $request->buy_id &&
                isset($request->order_id) && $request->order_id &&
                isset($request->email) && $request->email &&
                isset($request->number) && $request->number &&
                isset($request->code) && $request->code
            ){
                $test = payment_history::insert([
                    "buy_id" => $request->buy_id,
                    "order_id" => $request->order_id,
                    "status" => 'pennding',
                    "email"=>"$request->email",
                    "user_id"=>$user_id,
                    "phone"=>"$request->number",
                    "country"=>"$request->code",
                    "city"=>"",
                    "address"=>"",
                    "zip"=>"",
                    "first_name"=>"$first_name",
                    "last_name"=>"$last_name",
                    "middle_name"=>"$middle_name",
                    "birth_date"=>"$birth_date",
                    "citizenship"=>"$citizenship",
                    "expire"=>"$expire",
                    "extra_phone"=>"$extra",
                    "created_at"=>date("Y-m-d H:i:s"),
                    "updated_at"=>date("Y-m-d H:i:s")
                ]);
                return ['status' => 1];
            }else{return ['status' => 0, 'error' => 'error input field'];}
        }else{
            return ['status' => 0, 'error' => 'error Auth user'];
        }
    }
    public function payment_history_active(Request $request){
        payment_history::where('order_id', $request->order_id)
            ->update(['status' => 'active']);
        return ['status' => 1];
    }
    public function payment_history_bilet(Request $request){
        $bilet = stripcslashes($request->bilet);
        payment_history::where('order_id', $request->order_id)->update(['bilet' => $bilet]);
        return ['status' => $bilet];
    }
    public function postAjaxTickets(Request $request)
    {
        if(
            isset($request->request_id) && $request->request_id &&
            isset($request->segment) && $request->segment &&
            isset($request->directions) && $request->directions &&
            isset($request->buy_id) && $request->buy_id
        ){
            $segment = $request->segment;
            $directions = $request->directions;
            tickets::insert(["request_id" => $request->request_id, "json" => $segment, 'directions' => $directions, 'buy_id' => $request->buy_id]);
            return ['status' => 1];
        }else{
            return ['status' => 0];
        }
    }
    public function postAjaxTicketsResult(Request $request)
    {
        if(
            isset($request->request_id) && $request->request_id
        ){
            $tickets = tickets::where('request_id', $request->request_id)->first();
            $params_page['tickets'] = json_decode($tickets->json);
            $params_page['directions'] = json_decode($tickets->directions);
            return ['status' => 1, 'res' => $params_page];
        }else{
            return ['status' => 0];
        }
    }
}



