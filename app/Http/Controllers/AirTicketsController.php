<?php

namespace App\Http\Controllers;

use App\Category;
use App\payment_history;
use App\authkey;
use App\tickets;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Controllers\Auth\ChangePasswordController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Hash;
use Validator;

class AirTicketsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    /*Список городов*/
    public function arrCities(){
        $json_search = json_decode(file_get_contents(route('indexPage').'/'.env('JSON_AIRPORTS')));
        $lang = 'RU';$cities = [];
        foreach ($json_search as $search){
            $cities[$search->iata] = $search->names->$lang;
        }
        return $cities;
    }

    public function airTurnOn(){
        $client =  new Client(['verify' => false,"timeout" => 20]);
        $response = $client->request("POST", env("LOCO_PLANE_SERVICE")."login/", ["query" =>  ['login' => env("LOCO_LOGIN"), 'password' => env("LOCO_PASSWORD"), 'http_errors' => false]]);
        return json_decode($response->getBody())->etm_auth_key;
    }

    public function ajaxAirTickets(Request $request){

        $request_id = $etm_auth_key = '';$status = true;
        $etm_auth_key = $this->airTurnOn();
        if(!isset($etm_auth_key)){$status = false;}
        $raw['directions'][] = [ "departure_code" => "{$request->from_input}", "arrival_code" => "{$request->to_input}", "date" => "{$request->beginning}", "time" => "" ];
        if(isset($request->end) && $request->end){
            $raw['directions'][] = [ "departure_code" => "{$request->to_input}", "arrival_code" => "{$request->from_input}", "date" => "{$request->end}", "time" => "" ];
        }

        $raw['adult_qnt'] = $request->adults;
        $raw['child_qnt'] = $request->child;
        $raw['infant_qnt'] = $request->baby;
        $raw['passenger_category'] = 'ADT';
//        "AAT": "Трансферный",
//        "YTH": "Молодежь",
//        "YCD": "Пожилой"
//        "ADT": "это общая категория взрослого пассажира"
        $raw['class'] = $request->price_type;
        $raw['direct'] = false;
        $raw['flexible'] = true;
        $raw['min_price'] = 0;
        $raw['max_price'] = 10000000;
//        $raw['airlines'] =[];
        $raw['fare_types'] =["PUB","NEG"];
        $raw = json_encode($raw);

        $client =  new Client(['verify' => false,'base_uri' => env("LOCO_PLANE_SERVICE"),"timeout" => 20,"headers" => ["etm-auth-key" => $etm_auth_key],'body' => $raw]);
        $response = $client->request('POST', 'air/search');
        $request_id = json_decode($response->getBody())->request_id;
        authkey::insert([ "request_id" => $request_id, "etm_auth_key" => $etm_auth_key]);
        return response()->json(['request_id'=>$request_id], 200);
    }

    public function postAjaxSearchResult(Request $request){
        $status = 'off';
        $request_id = $request->request_id;
        $authkey = authkey::where('request_id', $request_id)->first();
        $etm_auth_key = $authkey->etm_auth_key;

        $client =  new Client([
            'verify' => false,
            "timeout" => 20,
            "headers" => [
                "etm-auth-key" => $etm_auth_key,
            ],
        ]);
        $response = $client->request("GET", env("LOCO_PLANE_SERVICE")."air/offers",["query" =>  ['request_id' => $request_id, 'http_errors' => false]]);
        $offers = json_decode($response->getBody())->offers;
        $directions = json_decode($response->getBody())->directions;
        if(isset($offers) && $offers) {$status = 'on';}

        return ['response'=>$response, 'fers'=>json_encode($offers), 'offers'=>$offers, 'request_id' => $request_id, 'directions' => $directions, 'status' => $status];
    }

    public function postAjaxSearchAvia(Request $request){
        $response = 111;
        return ['response'=>$response];
    }

    public function airTickets(Request $request)
    {
        $code['adults'] = $request->adults;
        $code['child'] = $request->child;
        $code['baby'] = $request->baby;
        $code['to_input'] = $request->to_input;
        $code['from_input'] = $request->from_input;

        $beginning = date("Y-m-d", time()+24*60*60*7);
        $end = '';
        if(isset($request->beginning)){$beginning=$request->beginning;}
        if(isset($request->end)){$end=$request->end;}
        return view('AirTickets',['cities'=>$this->arrCities(), 'beginning'=>$beginning, 'end'=>$end, 'code' => $code]);
    }

    public function postAjaxTickets(Request $request)
    {
        $segment = json_encode($request->segment);
        $directions = json_encode($request->directions);
        tickets::insert(["request_id" => $request->request_id, "json" => $segment, 'directions' => $directions]);
        return true;
    }

    public function buy(Request $request)
    {
        $params_page = [];
        $air = new AirTicketsController();
        $params_page['cities'] = $air->arrCities();
        $params_page['beginning'] = date("Y-m-d", time()+24*60*60*7);
        $params_page['end'] = '';
        $code['to_input'] = $request->to_input;
        $code['from_input'] = $request->from_input;
        $params_page['code'] = $code;
        $params_page['status'] = true;
        $params_page['messages'] = [];
        if (isset($request->buy_id) && $request->buy_id && isset($request->request_id) && $request->request_id ){
            $tickets = tickets::where('request_id', $request->request_id)->first();
            $params_page['tickets'] = json_decode($tickets->json);
            $params_page['directions'] = json_decode($tickets->directions);
            $authkey = authkey::where('request_id', $request->request_id)->first();
            $etm_auth_key = $authkey->etm_auth_key;
            $client2 =  new Client(["verify" => false,"timeout" => 20,"headers" => ["etm-auth-key" => $etm_auth_key]]);
            /*сильно затупляет код, проверяет на доступность*/
            $request_buy_id = $client2->request("GET", env("LOCO_PLANE_SERVICE")."air/offers/{$request->buy_id}/availability",['http_errors' => false]);
            if(isset(json_decode($request_buy_id->getBody())->status) && json_decode($request_buy_id->getBody())->status == 'ok' && isset(json_decode($request_buy_id->getBody())->availability) && json_decode($request_buy_id->getBody())->availability){
                $params_page['buy_id'] = $request->buy_id;
                $params_page['departure_country'] = $request->departure_country;
                $params_page['request_id'] = $request->request_id;
                if(isset($params_page['buy_id']) && isset($params_page['request_id'])){
                    DB::table('tickets')
                        ->where('request_id', $params_page['request_id'])
                        ->update(['buy_id' => $params_page['buy_id']]);
                }
            }else{$params_page['status'] = false;}
        }
        $session = $request->session()->all();
        $params_page['session'] = $session;
        if(!$params_page['status']){
            header('Location: /');
            exit;
        }
        $params_page['adults'] = $request->adults;
        $params_page['child'] = $request->child;
        $params_page['baby'] = $request->baby;
        $params_page['passengers'] = $request->passengers;

        return view('offer', $params_page);
    }

    public function valid_number($number)
    {
        $number = str_replace(' ', '', $number);
        $number = str_replace('+', '', $number);
        $number = str_replace('-', '', $number);
        $number = str_replace('(', '', $number);
        $number = str_replace(')', '', $number);
        $number = trim($number, " ");
        return $number;
    }

    public function buy_info(Request $request){

        $session = $request->session()->all();
        $this->session($request);
        $validator = $this->validator($request->all());
        $raw = [];
        for ($i = 1; $i <= $request->passengers; $i++) {
            $raw['passengers'][] = ["type"=>$request->type[$i], "gender"=>$request->gender[$i], "last_name"=>$request->last_name[$i], "first_name"=>$request->first_name[$i],
                "middle_name"=>$request->middle_name[$i], "birth_date"=>$request->birth_date[$i], "citizenship"=>$request->code,
                "document"=>["type"=>$request->Amadeus[$i], "number"=>$request->number_document[$i], "expire"=>$request->expire[$i]]
            ];
        }
        $status = true;
        $authkey = authkey::where('request_id', $request->request_id)->first();

        /*Добавляем данные в БД со статусом в ожидании*/
        $extra = $this->valid_number($request->extra);
        $number = $this->valid_number($request->number);

        $raw['buy_id'] = $request->buy_id;
        if(isset($extra) && $extra){$raw['phone'] = ["code"=>$request->code, "number"=>$number, "extra"=>$extra];}
        else{$raw['phone'] = ["code"=>$request->code, "number"=>$number];}
        $raw['emails'] = [$request->email];
//        $raw['address'] = ["country"=>$request->code];

        $raw = json_encode($raw);
        $client =  new Client([
            'verify' => false,
            "headers" => [
                "etm-auth-key" => $authkey->etm_auth_key,
            ],
            'body' => $raw
        ]);

        /*Создание заказа бронирование*/
        $response = $client->request("POST", env("LOCO_PLANE_SERVICE")."air/orders", ['http_errors' => false]);
        $bilet = json_decode($response->getBody());

        if(isset($bilet->status) && $bilet->status == 'ok'){
            if(isset($bilet->data->order_id) && $bilet->data->order_id){
                /*Записать в БД order_id*/
                $user_id = null;
                if (Auth::check()) {$user_id = Auth::user()->id;}
                $first_name=json_encode($request->first_name);
                $last_name=json_encode($request->last_name);
                $middle_name=json_encode($request->middle_name);
                $birth_date=json_encode($request->birth_date);
                $citizenship=json_encode($request->citizenship);
                $expire=json_encode($request->expire);
                payment_history::insert([
                    "buy_id" => $request->buy_id,
                    "order_id" => $bilet->data->order_id,
                    "status" => 'pennding',
                    "email"=>"$request->email",
                    "user_id"=>$user_id,
                    "phone"=>"$number",
                    "country"=>"$request->code",
                    "city"=>"",
                    "address"=>"",
                    "zip"=>"",
                    "first_name"=>"$first_name",
                    "last_name"=>"$last_name",
                    "middle_name"=>"$middle_name",
                    "birth_date"=>"$birth_date",
                    "citizenship"=>"$citizenship",
                    "expire"=>"$expire",
                    "extra_phone"=>"$extra"
                ]);
                if(isset($bilet->data->order_id)){
                    return redirect('order/'.$bilet->data->order_id);
                }else{
                    return view('error.error_offer',['messages'=>'Возможно эта бронь больше недействительна, попробуйте купить билет заново.']);
                }
            }else{return view('error.error_offer',['messages'=>'Возможно эта бронь больше недействительна, попробуйте купить билет заново.']);}
        }
//            }
//        }
        return view('error.error_offer',['messages'=>'Возможно эта бронь больше недействительна, попробуйте купить билет заново.']);
    }

    protected function order(Request $request, $order_id){
        /*нужно обернуть в метод*/
        $client =  new Client(['verify' => false, "timeout" => 20]);
        $response = $client->request("POST", env("LOCO_PLANE_SERVICE")."login/", ["query" =>  ['login' => env("LOCO_LOGIN"), 'password' => env("LOCO_PASSWORD"), 'http_errors' => false]]);
        $etm_auth_key = '';
        $status = $response_orders = false;

        if( isset(json_decode($response->getBody())->etm_auth_key) ) {$status = true; $etm_auth_key = json_decode($response->getBody())->etm_auth_key;}
        $result = null;
        if ($status){
            $client =  new Client(['verify' => false, "timeout" => 200000, "headers" => ["etm-auth-key" => $etm_auth_key]]);
            $result = $client->request("GET", env("LOCO_PLANE_SERVICE")."air/orders/{$order_id}", ['http_errors' => false])->getBody();
            $response_orders['order'] = json_decode($result);
        }

        payment_history::where('order_id', $response_orders['order']->data->order_id)->where('bilet', NULL)->update(['bilet' => $result]);
        $order = payment_history::where('order_id', $response_orders['order']->data->order_id)->first();
        $response_orders['status_buy'] = $order->status;

        return view('order', $response_orders);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'last_name[]' => 'required|alpha',
            'first_name[]' => 'required|alpha',
            'middle_name[]' => 'required|alpha',
            'birth_date[]' => 'sometimes|required',
            'citizenship[]' => 'required',
            'gender[]' => 'required',
            'expire[]' => 'required',
            'Amadeus[]' => 'required|alpha_dash',
            'number_document[]' => 'sometimes|required|alpha_dash',
            'number' => 'required',
            'extra' => 'sometimes|required',
            'email' => 'required|unique:users|email',
//            'zip' => 'sometimes|required|alpha_dash',
//            'country' => 'required',
//            'city' => 'required',
//            'additional' => 'required'
        ]);
    }

    protected function session($request)
    {
//        for ($i = 1; $i <= $request->passengers; $i++) {
            session([
                'last_name' => $request->last_name,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'birth_date' => $request->birth_date,
                'citizenship' => $request->citizenship,
                'expire' => $request->expire,
                'Amadeus' => $request->Amadeus,
                'number_document' => $request->number_document,
                'number' => $request->number,
                'extra' => $request->extra,
                'email' => $request->email,
                'gender' => $request->gender,
//                'zip' => $request->zip,
//                'country' => $request->country,
//                'city' => $request->city,
//                'additional' => $request->additional
            ]);
    }

    public function payment(Request $request){
        /*нужно обернуть в метод*/
        $client =  new Client([
            'verify' => false,
            "timeout" => 200000,
        ]);
        $response = $client->request("POST", env("LOCO_PLANE_SERVICE")."login/", ["query" =>  ['login' => env("LOCO_LOGIN"), 'password' => env("LOCO_PASSWORD"), 'http_errors' => false]]);
        $etm_auth_key = '';
        $status = $response_orders = false;
        if( isset(json_decode($response->getBody())->etm_auth_key) ) {$status = true; $etm_auth_key = json_decode($response->getBody())->etm_auth_key;}
        if ($status){
            $client =  new Client([
                'verify' => false,
                'base_uri' => env("LOCO_PLANE_SERVICE"),
                "timeout" => 200000,
                "headers" => [
                    "etm-auth-key" => $etm_auth_key,
                ],
            ]);
            $response_orders['payment'] = json_decode($client->request("POST", env("LOCO_PLANE_SERVICE")."air/orders/{$request->order_id}/payment", ['http_errors' => false, 'currency' => 'KZT', 'sender_sum' => $request->amount ])->getBody());
            payment_history::where('order_id', $request->order_id)->update(['status' => 'active']);
        }
        return response()->json(['status' => 'ok', 'response_orders' => $response_orders], 200);
    }


}
