<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $air = new AirTicketsController();
        return view('index', ['cities'=>$air->arrCities(), 'beginning'=>date("Y-m-d", time()+24*60*60*7)]);
    }

    public function getJson()
    {
        $url_data = [
            ['title'=>'fffff', 'url'=>'http://url.ru0'],
            ['title'=>'33333', 'url'=>'http://url2.ru']
        ];
        return $url_data;
    }

    public function corpLanding()
    {
        return view('corp-landing');
    }

    public function taplink()
    {
        return view('taplink');
    }

}
