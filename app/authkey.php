<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class authkey extends Model
{
    protected $table = "authkey";
    protected $primaryKey = 'request_id';
    protected $fillable = ['request_id', 'etm_auth_key'];
}
