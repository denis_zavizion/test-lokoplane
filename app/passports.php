<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class passports extends Model
{
    protected $table = "passports";
    protected $primaryKey = 'passport_id';
    protected $fillable = ['passport_id', 'passport', 'code'];
}
