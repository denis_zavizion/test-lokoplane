<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment_history extends Model
{
    protected $table = "payment_history";
    protected $fillable = ['buy_id', 'order_id', 'book_id', 'status', 'pnr_number', 'amount', 'email', 'user_id', 'phone', 'country', 'city', 'address', 'zip', 'first_name', 'last_name', 'middle_name', 'birth_date', 'citizenship', 'expire', 'extra_phone', 'bilet', 'created_at', 'updated_at'];
}
