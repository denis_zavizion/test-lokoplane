<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tickets extends Model
{
    protected $table = "tickets";
    protected $primaryKey = 'request_id';
    protected $fillable = ['request_id', 'json', 'directions', 'buy_id'];
}
