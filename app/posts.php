<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    protected $table = "posts";
    protected $primaryKey = 'post_id';
    protected $fillable = ['title', 'description', 'slug'];
}
